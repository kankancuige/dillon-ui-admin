package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.operatelog.OperateLogRespVO;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

/**
 * 操作日志记录
 *
 * @author ruoyi
 */
public interface SysOperlogFeign extends FeignAPI {
    //查看操作日志分页列表
    @RequestLine("GET /system/operate-log/page")
    CommonResult<PageResult<OperateLogRespVO>> pageOperateLog(@QueryMap Map<String, Object> params);


}
