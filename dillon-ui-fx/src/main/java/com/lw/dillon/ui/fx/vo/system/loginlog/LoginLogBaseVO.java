package com.lw.dillon.ui.fx.vo.system.loginlog;

import lombok.Data;

/**
 * 登录日志 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class LoginLogBaseVO {

    private Integer logType;

    private String traceId;

    private String username;

    private Integer result;

    private String userIp;

    private String userAgent;

}
