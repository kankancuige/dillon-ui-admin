package com.lw.dillon.ui.fx.vo.system.tenant;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 租户 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class TenantBaseVO {

    private String name;

    private String contactName;

    private String contactMobile;

    private Integer status;

    private String domain;

    private Long packageId;

    private LocalDateTime expireTime;

    private Integer accountCount;

}
