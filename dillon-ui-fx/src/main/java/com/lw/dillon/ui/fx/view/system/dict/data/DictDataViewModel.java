package com.lw.dillon.ui.fx.view.system.dict.data;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDictDataFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysDictTypeFeign;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeSimpleRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;
import java.util.Map;

public class DictDataViewModel implements ViewModel {

    private ObservableList<DictDataRespVO> dictDataList = FXCollections.observableArrayList();
    private ObservableList<DictTypeSimpleRespVO> dictTypeList = FXCollections.observableArrayList();

    private ObjectProperty<Integer> status = new SimpleObjectProperty<>();
    private StringProperty label = new SimpleStringProperty("");
    private ObjectProperty<DictTypeSimpleRespVO> dictType = new SimpleObjectProperty<>();

    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);

    private BooleanProperty loading = new SimpleBooleanProperty(false);

    public DictDataViewModel() {


    }


    public void initData(String dictType) {
        Map<String, Object> querMap = new HashMap<>();
        querMap.put("label", label.getValue());
        querMap.put("dictType", dictType);
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create()
                .addRunnableInPlatformThread(()->loading.set(true))
                .addSupplierInExecutor(() -> Request.connector(SysDictTypeFeign.class).getSimpleDictTypeList().getCheckedData())
                .addConsumerInPlatformThread(rel -> dictTypeList.addAll(rel))
                .addSupplierInExecutor(() ->
                        Request.connector(SysDictDataFeign.class).getDictTypePage(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(rel -> {
                    total.setValue(rel.getTotal().intValue());
                    dictDataList.addAll(rel.getList());
                })
                .addRunnableInPlatformThread(() -> {
                    DictTypeSimpleRespVO simpleRespVO = CollUtil.findOne(dictTypeList, item -> item.getType().equals(dictType));
                    dictTypeProperty().set(simpleRespVO);
                })
                .withFinal(()->loading.set(false))
                .onException(e -> e.printStackTrace()).run();

    }

    public void updateData() {

        Map<String, Object> querMap = new HashMap<>();
        querMap.put("label", label.getValue());
        if (ObjectUtil.isNotNull(dictType.getValue())) {
            querMap.put("dictType", dictType.getValue().getType());
        }
        querMap.put("status", status.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create().addRunnableInPlatformThread(() -> dictDataList.clear())
                .addSupplierInExecutor(() ->
                        Request.connector(SysDictDataFeign.class).getDictTypePage(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(rel -> {
                    total.setValue(rel.getTotal().intValue());
                    dictDataList.addAll(rel.getList());
                })

                .onException(e -> e.printStackTrace()).run();

    }

    public CommonResult<Boolean> deleteDictData(Long id) {
        return Request.connector(SysDictDataFeign.class).deleteDictData(id);
    }


    public void reset() {
        label.setValue(null);
        status.setValue(null);
        dictType.set(null);
        updateData();
    }

    public ObservableList<DictDataRespVO> getDictDataList() {
        return dictDataList;
    }

    public void setDictDataList(ObservableList<DictDataRespVO> dictDataList) {
        this.dictDataList = dictDataList;
    }

    public Integer getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }


    public String getLabel() {
        return label.get();
    }

    public StringProperty labelProperty() {
        return label;
    }

    public void setLabel(String label) {
        this.label.set(label);
    }

    public DictTypeSimpleRespVO getDictType() {
        return dictType.get();
    }

    public ObjectProperty<DictTypeSimpleRespVO> dictTypeProperty() {
        return dictType;
    }

    public void setDictType(DictTypeSimpleRespVO dictType) {
        this.dictType.set(dictType);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<DictTypeSimpleRespVO> getDictTypeList() {
        return dictTypeList;
    }

    public void setDictTypeList(ObservableList<DictTypeSimpleRespVO> dictTypeList) {
        this.dictTypeList = dictTypeList;
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
