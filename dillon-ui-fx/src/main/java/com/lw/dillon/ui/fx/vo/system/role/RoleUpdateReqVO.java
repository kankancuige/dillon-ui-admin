package com.lw.dillon.ui.fx.vo.system.role;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleUpdateReqVO extends RoleBaseVO {

    private Long id;
    private Integer status;

}
