package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantPageReqVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantRespVO;
import com.lw.dillon.ui.fx.vo.system.tenant.TenantUpdateReqVO;
import feign.Param;
import feign.RequestLine;

public interface TenantFeign extends FeignAPI {
    //使用租户名，获得租户编号", description = "登录界面，根据用户的租户名，获得租户编号
    @RequestLine("GET /system/tenant/get-id-by-name?name={name}")
    public CommonResult<Long> getTenantIdByName(@Param("name") String name);

    //创建租户
    @RequestLine("POST /system/tenant/create")
    public CommonResult<Long> createTenant(TenantCreateReqVO createReqVO);

    //更新租户
    @RequestLine("PUT /system/tenant/update")
    public CommonResult<Boolean> updateTenant(TenantUpdateReqVO updateReqVO);

    //删除租户
    @RequestLine("DELETE /system/tenant/delete")
    public CommonResult<Boolean> deleteTenant(@Param("id") Long id);

    //获得租户
    @RequestLine("GET /system/tenant/get")
    public CommonResult<TenantRespVO> getTenant(@Param("id") Long id);

    //获得租户分页
    @RequestLine("GET /system/tenant/page")
    public CommonResult<PageResult<TenantRespVO>> getTenantPage(TenantPageReqVO pageVO);


}
