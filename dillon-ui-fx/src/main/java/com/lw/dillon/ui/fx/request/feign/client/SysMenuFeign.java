package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.menu.*;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;

public interface SysMenuFeign extends FeignAPI {


    //创建菜单
    @RequestLine("POST /system/menu/create")
    CommonResult<Long> createMenu(MenuCreateReqVO reqVO);

    //修改菜单
    @RequestLine("PUT /system/menu/update")
    CommonResult<Boolean> updateMenu(MenuUpdateReqVO reqVO);


    //删除菜单
    @RequestLine("DELETE /system/menu/delete?id={id}")
    CommonResult<Boolean> deleteMenu(@Param("id") Long id);

    //获取菜单列表", description = "用于【菜单管理】界面
    @RequestLine("GET /system/menu/list")
    CommonResult<List<MenuRespVO>> getMenuList(@QueryMap MenuListReqVO reqVO);

    //获取菜单精简信息列表", description = "只包含被开启的菜单，用于【角色分配菜单】功能的选项。在多租户的场景下，会只返回租户所在套餐有的菜单
    @RequestLine("GET /system/menu/list-all-simple")
    CommonResult<List<MenuSimpleRespVO>> getSimpleMenuList();

    //获取菜单信息")
    @RequestLine("GET /system/menu/get")
    CommonResult<MenuRespVO> getMenu(Long id);

}
