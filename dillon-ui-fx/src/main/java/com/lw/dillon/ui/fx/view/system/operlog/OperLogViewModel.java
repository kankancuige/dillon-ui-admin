package com.lw.dillon.ui.fx.view.system.operlog;

import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysOperlogFeign;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.operatelog.OperateLogRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class OperLogViewModel implements ViewModel {


    private StringProperty module = new SimpleStringProperty();
    private StringProperty userNickname = new SimpleStringProperty();
    private ObjectProperty<Integer> type = new SimpleObjectProperty<>();
    private ObjectProperty<Boolean> success = new SimpleObjectProperty<>();
    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private ObjectProperty<LocalDate> startDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> endDate = new SimpleObjectProperty();
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);
    private ObservableList<OperateLogRespVO> operateLogList = FXCollections.observableArrayList();

    private BooleanProperty loading = new SimpleBooleanProperty(false);
    private Map<String, DictDataSimpleRespVO> typeMap;

    public OperLogViewModel() {
        initData();
    }

    private void initData() {
        updateData();
    }

    public void updateData() {


        Map<String, Object> querMap = new HashMap<>();

        if (ObjectUtil.isAllNotEmpty(startDate.getValue(), endDate.getValue())) {
            querMap.put("startTime", new String[]{
                    startDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 00:00:00",
                    endDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 23:59:59"
            });
        }
        querMap.put("module", module.getValue());
        querMap.put("userNickname", userNickname.getValue());
        querMap.put("type", type.getValue());
        querMap.put("success", success.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create().addRunnableInPlatformThread(() -> {
                            loading.set(true);
                            operateLogList.clear();
                        }
                )

                .addSupplierInExecutor(() ->
                        Request.connector(SysOperlogFeign.class).pageOperateLog(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(r -> {
                    typeMap = DictStore.getDictDataMap(DictTypeEnum.SYSTEM_OPERATE_TYPE.getDictType());
                    total.set(r.getTotal().intValue());
                    operateLogList.addAll(r.getList());
                })
                .withFinal(() -> loading.set(false))
                .onException(e -> e.printStackTrace()).run();
    }

    public void reset() {
        module.setValue(null);
        userNickname.setValue(null);
        success.setValue(null);
        type.setValue(null);
        endDate.setValue(null);
        startDate.setValue(null);
        updateData();
    }


    public String getModule() {
        return module.get();
    }

    public StringProperty moduleProperty() {
        return module;
    }

    public void setModule(String module) {
        this.module.set(module);
    }

    public String getUserNickname() {
        return userNickname.get();
    }

    public StringProperty userNicknameProperty() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname.set(userNickname);
    }

    public Integer getType() {
        return type.get();
    }

    public ObjectProperty<Integer> typeProperty() {
        return type;
    }

    public void setType(Integer type) {
        this.type.set(type);
    }

    public Boolean getSuccess() {
        return success.get();
    }

    public ObjectProperty<Boolean> successProperty() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success.set(success);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate() {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.set(endDate);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<OperateLogRespVO> getOperateLogList() {
        return operateLogList;
    }

    public void setOperateLogList(ObservableList<OperateLogRespVO> operateLogList) {
        this.operateLogList = operateLogList;
    }

    public Map<String, DictDataSimpleRespVO> getTypeMap() {
        return typeMap;
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
