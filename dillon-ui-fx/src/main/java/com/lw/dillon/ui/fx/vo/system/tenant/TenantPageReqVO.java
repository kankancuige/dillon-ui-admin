package com.lw.dillon.ui.fx.vo.system.tenant;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TenantPageReqVO extends PageParam {

    private String name;

    private String contactName;
    private String contactMobile;

    private Integer status;

    private LocalDateTime[] createTime;

}
