package com.lw.dillon.ui.fx.store;

import org.kordamp.ikonli.feather.Feather;

import java.util.Random;

public class AppStore {
    protected static final Random RANDOM = new Random();

    private static String token;
    private static Long tenantId;

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        AppStore.token = token;
    }


    public static Feather randomIcon() {
        return Feather.values()[RANDOM.nextInt(Feather.values().length)];
    }

    public static Long getTenantId() {
        return tenantId;
    }

    public static void setTenantId(Long tenantId) {
        AppStore.tenantId = tenantId;
    }
}
