package com.lw.dillon.ui.fx.vo.system.dict.data;

import lombok.Data;

/**
 * 字典数据 Excel 导出响应 VO
 */
@Data
public class DictDataExcelVO {

    private Long id;

    private Integer sort;

    private String label;

    private String value;

    private String dictType;

    private Integer status;

}
