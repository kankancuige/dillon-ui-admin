package com.lw.dillon.ui.fx.vo.system.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileRespVO extends UserBaseVO {

    private Long id;

    private Integer status;

    private String loginIp;

    private LocalDateTime loginDate;

    private LocalDateTime createTime;

    /**
     * 所属角色
     */
    private List<Role> roles;

    /**
     * 所在部门
     */
    private Dept dept;

    /**
     * 所属岗位数组
     */
    private List<Post> posts;
    /**
     * 社交用户数组
     */
    private List<SocialUser> socialUsers;

    @Data
    public static class Role {

        private Long id;

        private String name;

    }

    @Data
    public static class Dept {

        private Long id;

        private String name;

    }

    @Data
    public static class Post {

        private Long id;

        private String name;

    }

    @Data
    public static class SocialUser {

        private Integer type;

        private String openid;

    }

}
