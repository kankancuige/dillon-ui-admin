package com.lw.dillon.ui.fx.vo.system.role;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class RolePageReqVO extends PageParam {

    private String name;

    private String code;

    private Integer status;

    private LocalDateTime[] createTime;

}
