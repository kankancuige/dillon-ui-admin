package com.lw.dillon.ui.fx.request.feign.client;


import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeRespVO;
import com.lw.dillon.ui.fx.vo.system.notice.NoticeUpdateReqVO;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.Map;

/**
 * 公告 信息操作处理
 *
 * @author ruoyi
 */
public interface SysNoticeFeign extends FeignAPI {
    //创建通知公告
    @RequestLine("POST /system/notice/create")
    CommonResult<Long> createNotice(NoticeCreateReqVO reqVO);

    //修改通知公告
    @RequestLine("PUT /system/notice/update")
    CommonResult<Boolean> updateNotice(NoticeUpdateReqVO reqVO);

    //删除通知公告
    @RequestLine("DELETE /system/notice/delete?id={id}")
    CommonResult<Boolean> deleteNotice(@Param("id") Long id);

    //获取通知公告列表
    @RequestLine("GET /system/notice/page")
    CommonResult<PageResult<NoticeRespVO>> getNoticePage(@QueryMap Map<String, Object> queryMap);

    //获得通知公告
    @RequestLine("GET /system/notice/get?id={id}")
    CommonResult<NoticeRespVO> getNotice(@Param("id") Long id);
}
