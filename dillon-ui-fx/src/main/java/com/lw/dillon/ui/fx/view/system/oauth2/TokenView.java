package com.lw.dillon.ui.fx.view.system.oauth2;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.NumberUtil;
import com.lw.dillon.ui.fx.enums.DictTypeEnum;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.util.NodeUtils;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.view.control.PagingControl;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.token.OAuth2AccessTokenRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;
import static atlantafx.base.theme.Tweaks.*;

public class TokenView implements FxmlView<TokenViewModel>, Initializable {

    @InjectViewModel
    private TokenViewModel viewModel;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, String> accessTokenCol;

    @FXML
    private TextField clientId;

    @FXML
    private VBox contentPane;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, LocalDateTime> createTimeCol;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, LocalDateTime> expiresTimeCol;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, String> optCol;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, String> refreshTokenCol;

    @FXML
    private Button resetBut;

    @FXML
    private StackPane rootPane;

    @FXML
    private Button searchBut;

    @FXML
    private TableView<OAuth2AccessTokenRespVO> tableView;

    @FXML
    private TextField userId;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, Long> userIdCol;

    @FXML
    private ComboBox<DictDataSimpleRespVO> userType;

    @FXML
    private TableColumn<OAuth2AccessTokenRespVO, Integer> userTypeCol;
    private RingProgressIndicator loading;

    private ModalPane modalPane;
    private PagingControl pagingControl;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        modalPane = new ModalPane();
        modalPane.setId("tokenModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        rootPane.getChildren().add(modalPane);
        pagingControl = new PagingControl();
        contentPane.getChildren().add(pagingControl);
        pagingControl.totalProperty().bindBidirectional(viewModel.totalProperty());
        viewModel.pageNumProperty().bind(pagingControl.pageNumProperty());
        viewModel.pageSizeProperty().bindBidirectional(pagingControl.pageSizeProperty());
        viewModel.pageNumProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        pagingControl.pageSizeProperty().addListener((observable, oldValue, newValue) -> {
            viewModel.updateData();
        });
        loading = new RingProgressIndicator();
        loading.disableProperty().bind(loading.visibleProperty().not());
        loading.visibleProperty().bindBidirectional(contentPane.disableProperty());
        contentPane.disableProperty().bind(viewModel.loadingProperty());

        rootPane.getChildren().add(loading);

        userId.textProperty().bindBidirectional(viewModel.userIdProperty());
        clientId.textProperty().bindBidirectional(viewModel.clientIdProperty());


        searchBut.setOnAction(event -> viewModel.updateData());
        searchBut.getStyleClass().addAll(ACCENT);

        resetBut.setOnAction(event -> viewModel.reset());


        userType.setConverter(new StringConverter<DictDataSimpleRespVO>() {
            @Override
            public String toString(DictDataSimpleRespVO dictDataSimpleRespVO) {
                if (dictDataSimpleRespVO != null) {
                    return dictDataSimpleRespVO.getLabel();
                }
                return null;
            }

            @Override
            public DictDataSimpleRespVO fromString(String s) {
                return null;
            }
        });
        userType.setItems(DictStore.getDictDataList(DictTypeEnum.USER_TYPE.getDictType()));
        userType.valueProperty().addListener((observableValue, dictDataSimpleRespVO, t1) -> {
            if (t1 != null) {
                viewModel.setUserType(NumberUtil.parseInt(t1.getValue()));
            }
        });
        viewModel.userTypeProperty().addListener((observableValue, integer, t1) -> {
            userType.setValue(viewModel.getTypeMap().get(t1 + ""));
        });
        accessTokenCol.setCellValueFactory(new PropertyValueFactory<>("accessToken"));
        refreshTokenCol.setCellValueFactory(new PropertyValueFactory<>("refreshToken"));
        userIdCol.setCellValueFactory(new PropertyValueFactory<>("userId"));
        userTypeCol.setCellValueFactory(new PropertyValueFactory<>("userType"));
        expiresTimeCol.setCellValueFactory(new PropertyValueFactory<>("expiresTime"));
        createTimeCol.setCellValueFactory(new PropertyValueFactory<>("createTime"));

        userTypeCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        state.setText(viewModel.getTypeMap().get(item + "").getLabel());
                        state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });


        optCol.setCellFactory(col -> {
            return new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {

                        Button editBut = new Button("强退");
                        editBut.setOnAction(event -> showDictDataInfoDialog(getTableRow().getItem()));
                        editBut.getStyleClass().addAll(FLAT, DANGER);

                        HBox box = new HBox(editBut);
                        box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                        setGraphic(box);
                    }
                }
            };
        });


        expiresTimeCol.setCellFactory(col -> new TableCell<>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    if (item != null) {
                        this.setText(LocalDateTimeUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                    }
                }

            }
        });

        createTimeCol.setCellFactory(col -> new TableCell<>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                } else {
                    if (item != null) {
                        this.setText(LocalDateTimeUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                    }
                }

            }

        });

        tableView.setItems(viewModel.getoAuth2AccessTokemList());
        tableView.getSelectionModel().setCellSelectionEnabled(false);
        for (TableColumn<?, ?> c : tableView.getColumns()) {
            NodeUtils.addStyleClass(c, ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT);
        }

    }


    private void showDictDataInfoDialog(OAuth2AccessTokenRespVO oAuth2AccessTokenRespVO) {

        var dialog = new ModalPaneDialog("#tokenModalPane", "系统提示", new Label("是否要强制退出用户编号为【" + oAuth2AccessTokenRespVO.getUserId() + "】的用户？"));
        var closeBtn = new Button("取消");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create().addRunnableInExecutor(() -> viewModel.deleteAccessToken(oAuth2AccessTokenRespVO.getAccessToken())).addRunnableInPlatformThread(() -> {
                dialog.close();
                viewModel.updateData();
            }).onException(e -> e.printStackTrace()).run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }


}
