package com.lw.dillon.ui.fx.vo.system.tenant;

import lombok.Data;

import java.time.LocalDateTime;


/**
 * 租户 Excel VO
 *
 * @author liwen
 */
@Data
public class TenantExcelVO {

    private Long id;

    private String name;

    private String contactName;

    private String contactMobile;

    private Integer status;

    private LocalDateTime createTime;

}
