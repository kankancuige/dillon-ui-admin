package com.lw.dillon.ui.fx.view.system.dept;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDeptFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysUserFeign;
import com.lw.dillon.ui.fx.vo.system.dept.DeptCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dept.DeptRespVO;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dept.DeptUpdateReqVO;
import com.lw.dillon.ui.fx.vo.system.user.UserSimpleRespVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;
import java.util.Map;

/**
 * 部门对话框视图模型
 *
 * @author wenli
 * @date 2023/02/12
 */
public class DeptDialogViewModel implements ViewModel, SceneLifecycle {
    public final static String ON_CLOSE = "close";

    private ObservableList<UserSimpleRespVO> userlist = FXCollections.observableArrayList();
    private ObservableList<DeptSimpleRespVO> deptList = FXCollections.observableArrayList();

    private ObjectProperty<DeptSimpleRespVO> selectDeptProperty = new SimpleObjectProperty<>(new DeptSimpleRespVO());
    private Map<Long, UserSimpleRespVO> userMap = new HashMap<>();

    /**
     * 包装器
     */
    private ModelWrapper<DeptRespVO> wrapper = new ModelWrapper<>();


    public void initialize() {


    }

    private void setDept(DeptRespVO dept) {
        wrapper.set(dept);
        wrapper.reload();
    }

    public void initData(Long id) {
        ProcessChain.create()
                .addSupplierInExecutor(() -> Request.connector(SysUserFeign.class).getSimpleUserList().getCheckedData())
                .addConsumerInPlatformThread(listCommonResult -> userlist.addAll(listCommonResult))
                .addSupplierInExecutor(() -> Request.connector(SysDeptFeign.class).getSimpleDeptList().getCheckedData())
                .addConsumerInPlatformThread(rel -> deptList.addAll(rel))
                .addSupplierInExecutor(() -> {
                    if (ObjectUtil.isNotNull(id)) {
                        return Request.connector(SysDeptFeign.class).getDept(id).getCheckedData();
                    } else {
                        DeptRespVO respVO = new DeptRespVO();
                        respVO.setId(0l);
                        respVO.setStatus(0);
                        return respVO;
                    }

                })
                .addConsumerInPlatformThread(deptRespVOCommonResult -> setDept(deptRespVOCommonResult))

                .withFinal(() -> publish("initData"))
                .onException(e -> e.printStackTrace())
                .run();

    }


    public ObjectProperty<Long> parentIdProperty() {
        return wrapper.field("parentId", DeptRespVO::getParentId, DeptRespVO::setParentId, null);
    }

    public StringProperty nameProperty() {
        return wrapper.field("name", DeptRespVO::getName, DeptRespVO::setName, "");
    }

    public ObjectProperty<Integer> statusProperty() {
        return wrapper.field("status", DeptRespVO::getStatus, DeptRespVO::setStatus, null);
    }

    public StringProperty emailProperty() {
        return wrapper.field("email", DeptRespVO::getEmail, DeptRespVO::setEmail, null);
    }

    public StringProperty phoneProperty() {
        return wrapper.field("phone", DeptRespVO::getPhone, DeptRespVO::setPhone, null);
    }

    public ObjectProperty<Integer> sortProperty() {
        return wrapper.field("sort", DeptRespVO::getSort, DeptRespVO::setSort, 0).asObject();
    }

    public ObjectProperty<Long> leaderUserIdProperty() {
        return wrapper.field("leaderUserId", DeptRespVO::getLeaderUserId, DeptRespVO::setLeaderUserId, null);
    }

    public ObservableList<UserSimpleRespVO> getUserlist() {
        return userlist;
    }

    public void setUserlist(ObservableList<UserSimpleRespVO> userlist) {
        this.userlist = userlist;
    }

    public ObservableList<DeptSimpleRespVO> getDeptList() {
        return deptList;
    }

    public DeptSimpleRespVO getSelectDeptProperty() {
        return selectDeptProperty.get();
    }

    public ObjectProperty<DeptSimpleRespVO> selectDeptPropertyProperty() {
        return selectDeptProperty;
    }

    public void setSelectDeptProperty(DeptSimpleRespVO selectDeptProperty) {
        this.selectDeptProperty.set(selectDeptProperty);
    }

    public void setDeptList(ObservableList<DeptSimpleRespVO> deptList) {
        this.deptList = deptList;
    }


    public Map<Long, UserSimpleRespVO> getUserMap() {
        if (userMap.isEmpty()) {
            userlist.forEach(userSimpleRespVO -> userMap.put(userSimpleRespVO.getId(), userSimpleRespVO));
        }
        return userMap;
    }

    public CommonResult<Boolean> updateDept() {
        wrapper.commit();
        DeptRespVO dept = wrapper.get();
        DeptUpdateReqVO deptUpdateReqVO = new DeptUpdateReqVO();
        BeanUtil.copyProperties(dept, deptUpdateReqVO);
        deptUpdateReqVO.setParentId(selectDeptProperty.get().getId());
        return Request.connector(SysDeptFeign.class).updateDept(deptUpdateReqVO);

    }

    public CommonResult<Long> addDept() {
        wrapper.commit();
        DeptRespVO dept = wrapper.get();
        DeptCreateReqVO deptCreateReqVO = new DeptCreateReqVO();
        BeanUtil.copyProperties(dept, deptCreateReqVO);
        deptCreateReqVO.setParentId(selectDeptProperty.get().getId());
        return Request.connector(SysDeptFeign.class).createDept(deptCreateReqVO);

    }

    @Override
    public void onViewAdded() {

    }

    @Override
    public void onViewRemoved() {

    }


}
