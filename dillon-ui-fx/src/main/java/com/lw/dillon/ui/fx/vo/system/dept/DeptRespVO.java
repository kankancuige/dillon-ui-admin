package com.lw.dillon.ui.fx.vo.system.dept;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class DeptRespVO extends DeptBaseVO {

    private Long id;


    private LocalDateTime createTime;

}
