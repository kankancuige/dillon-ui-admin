package com.lw.dillon.ui.fx.vo.system.dept;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeptSimpleRespVO {

    private Long id;

    private String name;

    private Long parentId;

}
