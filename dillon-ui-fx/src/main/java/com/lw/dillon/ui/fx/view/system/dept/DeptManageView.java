package com.lw.dillon.ui.fx.view.system.dept;

import atlantafx.base.controls.ModalPane;
import atlantafx.base.controls.RingProgressIndicator;
import atlantafx.base.theme.Tweaks;
import cn.hutool.core.date.DateUtil;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysUserFeign;
import com.lw.dillon.ui.fx.view.control.ModalPaneDialog;
import com.lw.dillon.ui.fx.vo.system.dept.DeptRespVO;
import com.lw.dillon.ui.fx.vo.system.user.UserSimpleRespVO;
import de.saxsys.mvvmfx.FluentViewLoader;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.ViewTuple;
import io.datafx.core.concurrent.ProcessChain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.*;

/**
 * 部门管理视图
 *
 * @author wenli
 * @date 2023/02/15
 */
public class DeptManageView implements FxmlView<DeptManageViewModel>, Initializable {

    @InjectViewModel
    private DeptManageViewModel viewModel;
    @FXML
    private Button addBut;

    @FXML
    private VBox content;

    @FXML
    private TreeTableColumn<DeptRespVO, LocalDateTime> createTimeCol;

    @FXML
    private ToggleButton expansionBut;

    @FXML
    private TreeTableColumn<DeptRespVO, Long> leaderCol;

    @FXML
    private TreeTableColumn<DeptRespVO, String> nameCol;

    @FXML
    private TreeTableColumn<DeptRespVO, Long> optCol;

    @FXML
    private Button restBut;

    @FXML
    private StackPane root;

    @FXML
    private Button searchBut;

    @FXML
    private TreeTableColumn<DeptRespVO, Integer> sortCol;

    @FXML
    private TreeTableColumn<DeptRespVO, Integer> statusCol;

    @FXML
    private ComboBox<Integer> statusCombo;

    @FXML
    private TextField titleTextField;

    @FXML
    private TreeTableView<DeptRespVO> treeTableView;
    private RingProgressIndicator load;

    private ModalPane modalPane;

    /**
     * 初始化
     *
     * @param url            url
     * @param resourceBundle 资源包
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        modalPane = new ModalPane();
        modalPane.setId("deptModalPane");
        // reset side and transition to reuse a single modal pane between different examples
        modalPane.displayProperty().addListener((obs, old, val) -> {
            if (!val) {
                modalPane.setAlignment(Pos.CENTER);
                modalPane.usePredefinedTransitionFactories(null);
            }
        });
        root.getChildren().add(modalPane);

        treeTableView.setShowRoot(false);
        load = new RingProgressIndicator();
        load.disableProperty().bind(load.visibleProperty().not());
        load.visibleProperty().bindBidirectional(content.disableProperty());
        root.getChildren().add(load);

        addBut.setOnAction(event -> showAddDialog());
        addBut.getStyleClass().addAll(BUTTON_OUTLINED, ACCENT);
        titleTextField.textProperty().bindBidirectional(viewModel.titleProperty());
        statusCombo.valueProperty().bindBidirectional(viewModel.statusProperty());
        searchBut.setOnAction(event -> query());
        searchBut.getStyleClass().addAll(ACCENT);

        restBut.setOnAction(event -> {
            viewModel.rest();
            query();
        });
        expansionBut.selectedProperty().addListener((observable, oldValue, newValue) -> treeExpandedAll(treeTableView.getRoot(), newValue));
        nameCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
        sortCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("sort"));
        statusCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("status"));
        leaderCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("leaderUserId"));
        leaderCol.setCellFactory(tableView -> new TreeTableCell<>() {
            @Override
            protected void updateItem(Long aLong, boolean b) {
                super.updateItem(aLong, b);
                if (b || aLong == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    UserSimpleRespVO userSimpleRespVO = viewModel.getUserMap().get(aLong);
                    if (userSimpleRespVO != null) {
                        setText(userSimpleRespVO.getNickname());
                    }

                }
            }
        });
        leaderCol.setStyle("-fx-alignment: CENTER");
        sortCol.setStyle("-fx-alignment: CENTER");
        statusCol.setCellFactory(col -> {
            return new TreeTableCell<>() {
                @Override
                protected void updateItem(Integer item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        var state = new Label();
                        if (item == 0) {
                            state.setText("开启");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, SUCCESS);
                        } else {
                            state.setText("关闭");
                            state.getStyleClass().addAll(BUTTON_OUTLINED, DANGER);
                        }
                        HBox box = new HBox(state);
                        box.setPadding(new Insets(7, 7, 7, 7));
                        box.setAlignment(Pos.CENTER);
                        setGraphic(box);
                    }
                }
            };
        });
        createTimeCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("createTime"));
        createTimeCol.setStyle("-fx-alignment: CENTER");

        createTimeCol.setCellFactory(new Callback<TreeTableColumn<DeptRespVO, LocalDateTime>, TreeTableCell<DeptRespVO, LocalDateTime>>() {
            @Override
            public TreeTableCell<DeptRespVO, LocalDateTime> call(TreeTableColumn<DeptRespVO, LocalDateTime> param) {
                return new TreeTableCell<>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if (item != null) {
                                this.setText(DateUtil.format(item, "yyyy-MM-dd HH:mm:ss"));
                            }
                        }

                    }
                };
            }
        });

        optCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("id"));
        optCol.setCellFactory(new Callback<TreeTableColumn<DeptRespVO, Long>, TreeTableCell<DeptRespVO, Long>>() {
            @Override
            public TreeTableCell<DeptRespVO, Long> call(TreeTableColumn<DeptRespVO, Long> param) {

                TreeTableCell cell = new TreeTableCell<DeptRespVO, Long>() {
                    @Override
                    protected void updateItem(Long item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {

                            Button addBut = new Button("新增");
                            addBut.setOnAction(event -> showAddDialog());
                            addBut.setGraphic(FontIcon.of(Feather.PLUS));
                            addBut.getStyleClass().addAll(BUTTON_ICON, FLAT, SUCCESS);
                            Button editBut = new Button("修改");
                            editBut.setOnAction(event -> showEditDialog(getTableRow().getItem().getId()));
                            editBut.setGraphic(FontIcon.of(Feather.EDIT));
                            editBut.getStyleClass().addAll(BUTTON_ICON, FLAT, SUCCESS);
                            Button remBut = new Button("删除");
                            remBut.setOnAction(event -> showDelDialog(getTableRow().getItem()));
                            remBut.setGraphic(FontIcon.of(Feather.TRASH));
                            remBut.getStyleClass().addAll(BUTTON_ICON, FLAT, DANGER);
                            HBox box = new HBox(addBut, editBut, remBut);
                            box.setAlignment(Pos.CENTER);
//                            box.setSpacing(7);
                            setGraphic(box);
                        }
                    }
                };
                return cell;
            }
        });
        toggleStyleClass(treeTableView, Tweaks.ALT_ICON);


        query();
    }

    private TreeItem<DeptRespVO> generateTreeView(List<DeptRespVO> deptList, Long parentId) {
        TreeItem<DeptRespVO> rootNode = new TreeItem<>();
        rootNode.setExpanded(true);

        for (DeptRespVO dept : deptList) {
            if (dept.getParentId() == parentId) {
                TreeItem<DeptRespVO> childNode = new TreeItem<>(dept);
                childNode.getChildren().addAll(generateTreeView(deptList, dept.getId()).getChildren());
                rootNode.getChildren().add(childNode);
            }
        }

        return rootNode;
    }


    /**
     * 树扩展所有
     *
     * @param root     根
     * @param expanded 扩大
     */
    private void treeExpandedAll(TreeItem<DeptRespVO> root, boolean expanded) {
        for (TreeItem<DeptRespVO> child : root.getChildren()) {
            child.setExpanded(expanded);
            if (!child.getChildren().isEmpty()) {
                treeExpandedAll(child, expanded);
            }
        }
    }


    /**
     * 显示编辑对话框
     */
    private void showEditDialog(Long id) {
        ViewTuple<DeptDialogView, DeptDialogViewModel> load = FluentViewLoader.fxmlView(DeptDialogView.class).load();
        load.getViewModel().initData(id);


        var dialog = new ModalPaneDialog("#deptModalPane", "编辑部门", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create()
                    .addSupplierInExecutor(() -> load.getViewModel().updateDept())
                    .addConsumerInPlatformThread(rel -> {
                        if (rel.isSuccess()) {
                            dialog.close();
                            query();
                        }
                    }).onException(e -> e.printStackTrace())
                    .run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);


    }

    private void showAddDialog() {


        ViewTuple<DeptDialogView, DeptDialogViewModel> load = FluentViewLoader.fxmlView(DeptDialogView.class).load();
        load.getViewModel().initData(null);


        var dialog = new ModalPaneDialog("#deptModalPane", "添加部门", load.getView());
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create()
                    .addSupplierInExecutor(() -> load.getViewModel().addDept())
                    .addConsumerInPlatformThread(rel -> {
                        if (rel.isSuccess()) {
                            dialog.close();
                            query();
                        }
                    }).onException(e -> e.printStackTrace())
                    .run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);


    }


    /**
     * 显示del对话框
     *
     * @param deptRespVO 系统部门
     */
    private void showDelDialog(DeptRespVO deptRespVO) {

        var dialog = new ModalPaneDialog("#deptModalPane", "删除部门", new Label("是否确认删除名称为" + deptRespVO.getName() + "的数据项？"));
        var closeBtn = new Button("关闭");
        var okBtn = new Button("确定");
        closeBtn.setOnAction(evt -> dialog.close());
        okBtn.setOnAction(evt -> {
            ProcessChain.create()
                    .addSupplierInExecutor(() -> viewModel.deleteDept(deptRespVO.getId()))
                    .addConsumerInPlatformThread(rel -> {
                        if (rel.isSuccess()) {
                            dialog.close();
                            query();
                        }
                    }).onException(e -> e.printStackTrace())
                    .run();
        });
        HBox box = new HBox(10, closeBtn, okBtn);
        box.setAlignment(Pos.CENTER_RIGHT);
        dialog.addFooter(box);
        modalPane.show(dialog);
    }

    /**
     * 查询
     */
    private void query() {
        ProcessChain.create().addRunnableInPlatformThread(() -> load.setVisible(true))
                .addSupplierInExecutor(() -> {
                    List<UserSimpleRespVO> list = Request.connector(SysUserFeign.class).getSimpleUserList().getCheckedData();
                    Map<Long, UserSimpleRespVO> map = new HashMap<>();
                    for (UserSimpleRespVO user : list) {
                        map.put(user.getId(), user);
                    }
                    return map;
                })
                .addConsumerInPlatformThread(rel -> viewModel.getUserMap().putAll(rel))
                .addSupplierInExecutor(() -> viewModel.query())
                .addConsumerInPlatformThread(rel -> {
                    TreeItem<DeptRespVO> rootItem = generateTreeView(rel, 0l);
                    treeTableView.setRoot(rootItem);
                }).withFinal(() -> {
                    load.setVisible(false);
                }).onException(e -> e.printStackTrace())
                .run();
    }


}
