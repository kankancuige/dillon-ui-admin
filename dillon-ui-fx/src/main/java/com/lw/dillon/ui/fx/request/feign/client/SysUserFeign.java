package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.user.*;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

public interface SysUserFeign extends FeignAPI {

    //新增用户
    @RequestLine("POST /system/user/create")
    CommonResult<Long> createUser(UserCreateReqVO reqVO);

    //修改用户
    @RequestLine("PUT /system/user/update")
    CommonResult<Boolean> updateUser(UserUpdateReqVO reqVO);

    //删除用户
    @RequestLine("DELETE /system/user/delete?id={id}")
    CommonResult<Boolean> deleteUser(@Param("id") Long id);

    //重置用户密码
    @RequestLine("PUT /system/user/update-password")
    CommonResult<Boolean> updateUserPassword(UserUpdatePasswordReqVO reqVO);

    //修改用户状态
    @RequestLine("PUT /system/user/update-status")
    CommonResult<Boolean> updateUserStatus(UserUpdateStatusReqVO reqVO);

    //获得用户分页列表
    @RequestLine("GET /system/user/page")
    CommonResult<PageResult<UserPageItemRespVO>> getUserPage(@QueryMap Map<String, Object> querMap);

    //获取用户精简信息列表", description = "只包含被开启的用户，主要用于前端的下拉选项")
    @RequestLine("GET /system/user/list-all-simple")
    CommonResult<List<UserSimpleRespVO>> getSimpleUserList();

    //获得用户详情
    @RequestLine("GET /system/user/get?id={id}")
    CommonResult<UserRespVO> getUser(@Param("id") Long id);


    //获得导入用户模板
    @RequestLine("GET /system/user/get-import-template")
    void importTemplate();

    //导入用户")
//    @RequestLine("POST /system/user/import")
//     CommonResult<UserImportRespVO> importExcel(@Param("file") MultipartFile file, @Param(value = "updateSupport") Boolean updateSupport);


}
