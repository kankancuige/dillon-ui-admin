package com.lw.dillon.ui.fx.view.window;

import animatefx.animation.AnimateFXInterpolator;
import animatefx.animation.AnimationFX;
import animatefx.animation.FadeIn;
import animatefx.animation.FadeOut;
import animatefx.util.ParallelAnimationFX;
import atlantafx.base.theme.Tweaks;
import com.lw.dillon.ui.fx.icon.WIcon;
import com.lw.dillon.ui.fx.vo.system.auth.AuthPermissionInfoRespVO;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.Duration;
import org.kordamp.ikonli.feather.Feather;
import org.kordamp.ikonli.javafx.FontIcon;

import java.util.List;

import static atlantafx.base.theme.Styles.FLAT;

public class SideMenu extends StackPane {

    private TreeView<AuthPermissionInfoRespVO.MenuVO> treeView;
    private VBox menuBar;

    private MainViewModel mainViewModel;

    public SideMenu(MainViewModel mainViewModel) {
        this.mainViewModel = mainViewModel;
        mainViewModel.routersUpdateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                treeView.setRoot(createTreeItem());
            }
        });
        createView();
    }

    private void createView() {
        this.minWidthProperty().bind(this.prefWidthProperty());
        this.maxWidthProperty().bind(this.prefWidthProperty());
        menuBar = new VBox();
        menuBar.setId("side-menu-bar");
        menuBar.setMaxWidth(Double.MAX_VALUE);
        menuBar.setAlignment(Pos.TOP_CENTER);

        treeView = new TreeView();
        treeView.setShowRoot(false);
        treeView.getStyleClass().addAll(Tweaks.EDGE_TO_EDGE);
        treeView.setId("side-tree");
        treeView.setCellFactory(new Callback<TreeView<AuthPermissionInfoRespVO.MenuVO>, TreeCell<AuthPermissionInfoRespVO.MenuVO>>() {
            @Override
            public TreeCell call(TreeView param) {
                TreeCell treeCell = new TreeCell<AuthPermissionInfoRespVO.MenuVO>() {
                    @Override
                    protected void updateItem(AuthPermissionInfoRespVO.MenuVO item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {

                            Label label = new Label(item.getName());
                            String iconStr = item.getIcon();
                            label.setMaxWidth(Double.MAX_VALUE);
                            FontIcon icon = FontIcon.of(Feather.CHEVRON_DOWN);
                            label.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr), 24, Color.CYAN));
                            label.setGraphicTextGap(10);
                            HBox box = new HBox(label, icon);
                            HBox.setHgrow(label, Priority.ALWAYS);


                            icon.setVisible(!getTreeItem().isLeaf());
                            if (isSelected()) {
                                label.setId("side-menu-cell-selected");
                            }
                            box.setPadding(new Insets(7, 7, 7, 0));
                            setGraphic(box);
                        }

                    }
                };
                treeCell.setOnMouseClicked(event -> {
                    if (!treeCell.isEmpty()) {
                        treeCell.getTreeItem().setExpanded(!treeCell.getTreeItem().isExpanded());
                    }
                });
                return treeCell;
            }
        });
        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            TreeItem<AuthPermissionInfoRespVO.MenuVO> currentSelectItem = (TreeItem<AuthPermissionInfoRespVO.MenuVO>) newValue;
            if (currentSelectItem != null && currentSelectItem.isLeaf()) {
                mainViewModel.addTab(currentSelectItem.getValue());
            }
        });
        setId("side-menu");
        getChildren().addAll(menuBar, treeView);
    }

    private TreeItem<AuthPermissionInfoRespVO.MenuVO> createTreeItem() {
        var root = new TreeItem<AuthPermissionInfoRespVO.MenuVO>();
        root.setExpanded(true);

        mainViewModel.getRouterList().forEach(menuVO -> {
            var child = new TreeItem<AuthPermissionInfoRespVO.MenuVO>(menuVO);
            String iconStr = menuVO.getIcon();
            MenuButton menuButton = new MenuButton();
            menuButton.setPopupSide(Side.RIGHT);
            menuButton.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr), 32));
            menuButton.getStyleClass().addAll(FLAT, Tweaks.NO_ARROW);
            menuButton.setId("side-menu-button");

            var childObj = menuVO.getChildren();
            if (childObj != null) {
                generateTree(child, childObj);
                generateMenu(menuButton, childObj);
            } else {
                menuButton.setOnMouseClicked(event -> mainViewModel.addTab(menuVO));
            }

            Platform.runLater(() -> {
                root.getChildren().add(child);
                menuBar.getChildren().add(menuButton);
            });

        });

        return root;
    }

    private void generateTree(TreeItem<AuthPermissionInfoRespVO.MenuVO> parent, List<AuthPermissionInfoRespVO.MenuVO> menuVOList) {
        AuthPermissionInfoRespVO.MenuVO parentMenu=  parent.getValue();
        menuVOList.forEach(menuVO -> {
            menuVO.setPath(parentMenu.getPath()+"/"+menuVO.getPath());
            var child = new TreeItem<AuthPermissionInfoRespVO.MenuVO>(menuVO);
            var childObj = menuVO.getChildren();

            if (childObj != null) {
                generateTree(child, childObj);
            }

            Platform.runLater(() -> {
                parent.getChildren().add(child);

            });


        });
    }

    private void generateMenu(MenuButton parent, List<AuthPermissionInfoRespVO.MenuVO> jsonArray) {
        jsonArray.forEach(obj -> {

            var childObj =obj.getChildren();
            var text =obj.getName();
            String iconStr =obj.getIcon();
            if (childObj != null) {
                var child = new Menu(text);
                child.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr), 24));
                generateMenu2(child, childObj);

                Platform.runLater(() -> {
                    parent.getItems().add(child);

                });
            } else {
                var child = new MenuItem(text);
                child.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr), 32));
                child.setOnAction(event -> mainViewModel.addTab( obj));
                Platform.runLater(() -> {
                    parent.getItems().add(child);

                });
            }
            

        });
    }

    private void generateMenu2(Menu parent, List<AuthPermissionInfoRespVO.MenuVO> jsonArray) {
        jsonArray.forEach(obj -> {

          
                var text =obj.getName();
                String iconStr =obj.getIcon();
                var childObj =obj.getChildren();
                if (childObj != null) {
                    var child = new Menu(text);
                    child.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr), 24));
                    generateMenu2(child, childObj);

                    Platform.runLater(() -> {
                        parent.getItems().add(child);

                    });
                } else {
                    var child = new MenuItem(text);
                    child.setGraphic(FontIcon.of(WIcon.findByDescription("lw-" + iconStr)));
                    child.setOnAction(event -> mainViewModel.addTab(obj));
                    Platform.runLater(() -> {
                        parent.getItems().add(child);

                    });
                }
            

        });
    }

    public void expansion(boolean expansion) {


        if (expansion) {
            menuBar.setVisible(false);
            treeView.setVisible(true);
            AnimationFX animationFX = new FadeOut(this);
            Timeline timeline = new Timeline(new KeyFrame(Duration.millis(0),
                    new KeyValue(this.prefWidthProperty(), 50D, AnimateFXInterpolator.EASE)),
                    new KeyFrame(Duration.millis(200),
                            new KeyValue(this.prefWidthProperty(), 300D, AnimateFXInterpolator.EASE)));

            animationFX.setTimeline(timeline);
            ParallelAnimationFX parallelAnimationFX
                    = new ParallelAnimationFX(new FadeIn(treeView), animationFX);
            parallelAnimationFX.play();
        } else {
            treeView.setVisible(false);
            menuBar.setVisible(true);
            AnimationFX animationFX = new FadeOut(this);
            Timeline timeline = new Timeline(new KeyFrame(Duration.millis(0),
                    new KeyValue(this.prefWidthProperty(), 300D, AnimateFXInterpolator.EASE)),
                    new KeyFrame(Duration.millis(200),
                            new KeyValue(this.prefWidthProperty(), 50D, AnimateFXInterpolator.EASE)));

            animationFX.setTimeline(timeline);
            ParallelAnimationFX parallelAnimationFX
                    = new ParallelAnimationFX(new FadeIn(menuBar), animationFX);
            parallelAnimationFX.play();
        }
    }
}
