package com.lw.dillon.ui.fx.vo.system.dict.data;

import lombok.Data;

@Data
public class DictDataExportReqVO {

    private String label;

    private String dictType;

    private Integer status;

}
