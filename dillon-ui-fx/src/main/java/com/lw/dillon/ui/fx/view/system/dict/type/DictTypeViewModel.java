package com.lw.dillon.ui.fx.view.system.dict.type;

import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDictTypeFeign;
import com.lw.dillon.ui.fx.vo.system.dict.type.DictTypeRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class DictTypeViewModel implements ViewModel {

    private ObservableList<DictTypeRespVO> dictTypeList = FXCollections.observableArrayList();
    private ObjectProperty<LocalDate> startDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> endDate = new SimpleObjectProperty();
    private ObjectProperty<Integer> status = new SimpleObjectProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty type = new SimpleStringProperty();
    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);

    private BooleanProperty loding = new SimpleBooleanProperty(false);

    public DictTypeViewModel() {
        queryDictDataList();
    }

    public void queryDictDataList() {


        Map<String, Object> querMap = new HashMap<>();
        if (ObjectUtil.isAllNotEmpty(startDate.getValue(), endDate.getValue())) {
            String st = startDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 00:00:00";
            String et = endDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " 23:59:59";
            querMap.put("createTime", new String[]{st, et});
        }
        querMap.put("name", name.getValue());
        querMap.put("type", type.getValue());
        querMap.put("status", status.getValue());
        querMap.put("pageNum", pageNum.getValue() + 1);
        querMap.put("pageSize", pageSize.getValue());

        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    loding.set(true);
                    dictTypeList.clear();
                })
                .addSupplierInExecutor(() ->
                        Request.connector(SysDictTypeFeign.class).pageDictTypes(querMap).getCheckedData()
                )
                .addConsumerInPlatformThread(r -> {
                    total.set(r.getTotal().intValue());
                    dictTypeList.addAll(r.getList());
                })
                .withFinal(()->loding.set(false))
                .onException(e -> e.printStackTrace()).run();
    }

    public CommonResult<Boolean> deleteDictType(Long id) {
        return Request.connector(SysDictTypeFeign.class).deleteDictType(id);
    }

    public void reset() {
        name.setValue(null);
        status.setValue(null);
        type.setValue(null);
        endDate.setValue(null);
        startDate.setValue(null);
        queryDictDataList();
    }


    public ObservableList<DictTypeRespVO> getDictTypeList() {
        return dictTypeList;
    }

    public void setDictTypeList(ObservableList<DictTypeRespVO> dictTypeList) {
        this.dictTypeList = dictTypeList;
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate() {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.set(endDate);
    }

    public Integer getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public boolean isLoding() {
        return loding.get();
    }

    public BooleanProperty lodingProperty() {
        return loding;
    }

    public void setLoding(boolean loding) {
        this.loding.set(loding);
    }
}
