package com.lw.dillon.ui.fx.view.system.menu;

import cn.hutool.core.bean.BeanUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysMenuFeign;
import com.lw.dillon.ui.fx.vo.system.menu.MenuCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.menu.MenuRespVO;
import com.lw.dillon.ui.fx.vo.system.menu.MenuSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.menu.MenuUpdateReqVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/**
 * 菜单对话框视图模型
 *
 * @author wenli
 * @date 2023/02/12
 */
public class MenuDialogViewModel implements ViewModel, SceneLifecycle {
    private ObservableList<MenuSimpleRespVO> menuSimpleRespVOObservableList = FXCollections.observableArrayList();

    private ObjectProperty<MenuSimpleRespVO> selectMenu = new SimpleObjectProperty<>(new MenuSimpleRespVO());
    /**
     * 包装器
     */
    private ModelWrapper<MenuRespVO> wrapper = new ModelWrapper<>();


    public void initialize() {

        visibleProperty().addListener((observableValue, aBoolean, t1) -> {
            System.err.println(t1);
        });

        menuNameProperty().addListener((observableValue, aBoolean, t1) -> {
            System.err.println(t1);
        });


    }


    /**
     * 系统设置菜单
     */
    public void setMenu(MenuRespVO menuRespVO) {
        wrapper.set(menuRespVO);
        wrapper.reload();
    }

    /**
     * 父id属性
     *
     * @return {@link LongProperty}
     */
    public LongProperty parentIdProperty() {
        return wrapper.field("parentId", MenuRespVO::getParentId, MenuRespVO::setParentId, 0L);
    }


    /**
     * 菜单类型属性
     *
     * @return {@link StringProperty}
     */
    public IntegerProperty menuTypeProperty() {
        return wrapper.field("type", MenuRespVO::getType, MenuRespVO::setType, 0);
    }

    /**
     * 图标属性
     *
     * @return {@link StringProperty}
     */
    public StringProperty iconProperty() {
        return wrapper.field("icon", MenuRespVO::getIcon, MenuRespVO::setIcon, "");
    }

    /**
     * 菜单名称属性
     *
     * @return {@link StringProperty}
     */
    public StringProperty menuNameProperty() {
        return wrapper.field("name", MenuRespVO::getName, MenuRespVO::setName, "");
    }

    /**
     * 显示顺序属性
     *
     * @return {@link IntegerProperty}
     */
    public IntegerProperty sortProperty() {
        return wrapper.field("sort", MenuRespVO::getSort, MenuRespVO::setSort,0);
    }


    /**
     * 路由地址属性
     *
     * @return {@link StringProperty}
     */
    public StringProperty pathProperty() {
        return wrapper.field("path", MenuRespVO::getPath, MenuRespVO::setPath, "");
    }

    /**
     * 组件路径属性
     *
     * @return {@link StringProperty}
     */
    public StringProperty componentProperty() {
        return wrapper.field("component", MenuRespVO::getComponent, MenuRespVO::setComponent, "");
    }

    public StringProperty componentNameProperty() {
        return wrapper.field("componentName", MenuRespVO::getComponentName, MenuRespVO::setComponentName, "");
    }

    public StringProperty componentClassProperty() {
        return wrapper.field("componentClass", MenuRespVO::getComponentClass, MenuRespVO::setComponentClass, "");
    }

    /**
     * 权限字符串
     *
     * @return {@link StringProperty}
     */
    public StringProperty permsProperty() {
        return wrapper.field("permission", MenuRespVO::getPermission, MenuRespVO::setPermission, "");
    }




    /**
     * 可见属性
     *
     * @return {@link StringProperty}
     */
    public BooleanProperty visibleProperty() {
        return wrapper.field("visible", MenuRespVO::getVisible, MenuRespVO::setVisible, true);
    }

    public BooleanProperty alwaysShowProperty() {
        return wrapper.field("alwaysShow", MenuRespVO::getAlwaysShow, MenuRespVO::setAlwaysShow, true);
    }

    public BooleanProperty keepAliveProperty() {
        return wrapper.field("keepAlive", MenuRespVO::getKeepAlive, MenuRespVO::setKeepAlive, true);
    }

    /**
     * 状态属性
     *
     * @return {@link StringProperty}
     */
    public IntegerProperty statusProperty() {
        return wrapper.field("status", MenuRespVO::getStatus, MenuRespVO::setStatus, 0);
    }


    @Override
    public void onViewAdded() {

    }

    @Override
    public void onViewRemoved() {

    }

    public MenuSimpleRespVO getSelectMenu() {
        return selectMenu.get();
    }

    public ObjectProperty<MenuSimpleRespVO> selectMenuProperty() {
        return selectMenu;
    }

    public void setSelectMenu(MenuSimpleRespVO selectMenu) {
        this.selectMenu.set(selectMenu);
    }

    public ObservableList<MenuSimpleRespVO> getMenuSimpleRespVOObservableList() {
        return menuSimpleRespVOObservableList;
    }

    /**
     * 保存
     *
     * @param isEdit 是编辑
     * @return {@link Boolean}
     */
    public CommonResult update() {
        wrapper.commit();

        MenuUpdateReqVO saveMenu = new MenuUpdateReqVO();
        BeanUtil.copyProperties(wrapper.get(), saveMenu);
        saveMenu.setParentId(selectMenu.get().getId());
        CommonResult  result = Request.connector(SysMenuFeign.class).updateMenu(saveMenu);

        return result;
    }

    /**
     * 保存
     *
     * @param isEdit 是编辑
     * @return {@link Boolean}
     */
    public CommonResult add() {
        wrapper.commit();

        MenuCreateReqVO createMenu = new MenuCreateReqVO();
        BeanUtil.copyProperties(wrapper.get(), createMenu);
        createMenu.setParentId(selectMenu.get().getId());
        CommonResult result = Request.connector(SysMenuFeign.class).createMenu(createMenu);

        return result;
    }


    /**
     * 菜单列表
     */
    public void getMenu(Long id) {
        CommonResult<MenuRespVO> result = Request.connector(SysMenuFeign.class).getMenu(id);
        setMenu(result.getData());

    }

    public void getSimpleMenuList() {
        CommonResult<List<MenuSimpleRespVO>> commonResult = Request.connector(SysMenuFeign.class).getSimpleMenuList();
        getMenuSimpleRespVOObservableList().clear();
        getMenuSimpleRespVOObservableList().addAll(commonResult.getData());
    }
}
