package com.lw.dillon.ui.fx.vo.system.role;

import com.lw.dillon.framework.common.enums.CommonStatusEnum;
import com.lw.dillon.framework.common.validation.InEnum;
import lombok.Data;

@Data
public class RoleUpdateStatusReqVO {

    private Long id;

    @InEnum(value = CommonStatusEnum.class, message = "修改状态必须是 {value}")
    private Integer status;

}
