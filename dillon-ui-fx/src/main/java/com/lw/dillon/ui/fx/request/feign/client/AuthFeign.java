package com.lw.dillon.ui.fx.request.feign.client;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.auth.*;
import feign.Param;
import feign.RequestLine;

public interface AuthFeign extends FeignAPI {


    //使用账号密码登录
    @RequestLine("POST /system/auth/login")
    CommonResult<AuthLoginRespVO> login(AuthLoginReqVO reqVO);

    //登出系统
    @RequestLine("POST /system/auth/logout")
    CommonResult<Boolean> logout();


    //刷新令牌
    @RequestLine("POST /system/auth/refresh-token")
    CommonResult<AuthLoginRespVO> refreshToken(@Param("refreshToken") String refreshToken);

    //获取登录用户的权限信息
    @RequestLine("GET /system/auth/get-permission-info")
    CommonResult<AuthPermissionInfoRespVO> getPermissionInfo();

    // ========== 短信登录相关 ==========

    //使用短信验证码登录
    @RequestLine("POST /system/auth/sms-login")
    CommonResult<AuthLoginRespVO> smsLogin(AuthSmsLoginReqVO reqVO);


    //发送手机验证码
    @RequestLine("POST /system/auth/send-sms-code")
    CommonResult<Boolean> sendLoginSmsCode(AuthSmsSendReqVO reqVO);

    // ========== 社交登录相关 ==========

    //社交授权的跳转
    @RequestLine("GET /system/auth/social-auth-redirect")
    CommonResult<String> socialLogin(@Param("type") Integer type, @Param("redirectUri") String redirectUri);

    //社交快捷登录，使用 code 授权码", description = "适合未登录的用户，但是社交账号已绑定用户
    @RequestLine("POST /system/auth/social-login")
    CommonResult<AuthLoginRespVO> socialQuickLogin(AuthSocialLoginReqVO reqVO);
}
