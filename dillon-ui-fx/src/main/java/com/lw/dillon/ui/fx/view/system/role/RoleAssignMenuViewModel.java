package com.lw.dillon.ui.fx.view.system.role;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.PermissionFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysMenuFeign;
import com.lw.dillon.ui.fx.vo.system.menu.MenuSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.permission.PermissionAssignRoleMenuReqVO;
import com.lw.dillon.ui.fx.vo.system.role.RoleDO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.scene.control.TreeItem;

public class RoleAssignMenuViewModel implements ViewModel {

    // 角色名称
    private StringProperty name = new SimpleStringProperty();
    // 角色编码
    private StringProperty code = new SimpleStringProperty();
    // 角色ID
    private ObjectProperty<Long> id = new SimpleObjectProperty<>(null);
    // 选中的菜单ID集合
    private ObservableSet<Long> selMenuIdSet = FXCollections.observableSet();
    // 菜单列表
    private ObservableList<MenuSimpleRespVO> menuList = FXCollections.observableArrayList();
    private ObservableList<TreeItem<MenuSimpleRespVO>> selectTreeItems = FXCollections.observableArrayList();

    /**
     * 初始化
     */
    public void initializeData() {
    }

    /**
     * 分配角色菜单
     */
    public CommonResult<Boolean> assignRoleMenu() {
        PermissionAssignRoleMenuReqVO permissionAssignRoleMenuReqVO = new PermissionAssignRoleMenuReqVO();
        permissionAssignRoleMenuReqVO.setRoleId(getId());
        permissionAssignRoleMenuReqVO.setMenuIds(selMenuIdSet);
        return Request.connector(PermissionFeign.class).assignRoleMenu(permissionAssignRoleMenuReqVO);
    }

    /**
     * 初始化角色
     *
     * @param roleDO 角色DO
     */
    public void initializeData(RoleDO roleDO) {
        this.id.set(roleDO.getId());
        this.name.set(roleDO.getName());
        this.code.set(roleDO.getCode());

        ProcessChain.create()
                .addSupplierInExecutor(() -> Request.connector(SysMenuFeign.class).getSimpleMenuList().getCheckedData())
                .addConsumerInPlatformThread(menuSimpleRespVOS -> menuList.addAll(menuSimpleRespVOS))
                .addSupplierInExecutor(() -> Request.connector(PermissionFeign.class).getRoleMenuList(id.get()).getCheckedData())
                .addConsumerInExecutor(longs -> selMenuIdSet.addAll(longs))
                .onException(throwable -> throwable.printStackTrace())
                .withFinal(() -> publish("initData"))
                .run();
    }

    /**
     * 获取角色名称
     *
     * @return 角色名称
     */
    public String getName() {
        return name.get();
    }

    /**
     * 获取角色名称属性
     *
     * @return 角色名称属性
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * 设置角色名称
     *
     * @param name 角色名称
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * 获取角色编码
     *
     * @return 角色编码
     */
    public String getCode() {
        return code.get();
    }

    /**
     * 获取角色编码属性
     *
     * @return 角色编码属性
     */
    public StringProperty codeProperty() {
        return code;
    }

    /**
     * 设置角色编码
     *
     * @param code 角色编码
     */
    public void setCode(String code) {
        this.code.set(code);
    }

    /**
     * 获取角色ID
     *
     * @return 角色ID
     */
    public Long getId() {
        return id.get();
    }

    /**
     * 获取角色ID属性
     *
     * @return 角色ID属性
     */
    public ObjectProperty<Long> idProperty() {
        return id;
    }

    /**
     * 设置角色ID
     *
     * @param id 角色ID
     */
    public void setId(Long id) {
        this.id.set(id);
    }

    /**
     * 获取选中的菜单ID集合
     *
     * @return 选中的菜单ID集合
     */
    public ObservableSet<Long> getSelMenuIdSet() {
        return selMenuIdSet;
    }

    public ObservableList<MenuSimpleRespVO> getMenuList() {
        return menuList;
    }

    public ObservableList<TreeItem<MenuSimpleRespVO>> getSelectTreeItems() {
        return selectTreeItems;
    }

}
