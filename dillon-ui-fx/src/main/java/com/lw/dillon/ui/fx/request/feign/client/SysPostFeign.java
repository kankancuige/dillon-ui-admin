package com.lw.dillon.ui.fx.request.feign.client;


import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.ui.fx.request.feign.FeignAPI;
import com.lw.dillon.ui.fx.vo.system.post.*;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;

/**
 * 岗位信息操作处理
 *
 * @author ruoyi
 */

public interface SysPostFeign extends FeignAPI {


    //创建岗位
    @RequestLine("POST /system/post/create")
    public CommonResult<Long> createPost(PostCreateReqVO reqVO);

    //修改岗位
    @RequestLine("PUT /system/post/update")
    public CommonResult<Boolean> updatePost(PostUpdateReqVO reqVO);

    //删除岗位
    @RequestLine("DELETE /system/post/delete?id={id}")
    public CommonResult<Boolean> deletePost(@Param("id") Long id);

    //获取岗位精简信息列表", description = "只包含被开启的岗位，主要用于前端的下拉选项")
    @RequestLine("GET /system/post/list-all-simple")
    public CommonResult<List<PostSimpleRespVO>> getSimplePostList();

    //获得岗位信息
    @RequestLine("GET /system/post/get?id={id}")
    public CommonResult<PostRespVO> getPost(@Param("id") Long id);

    //获得岗位分页列表
    @RequestLine("GET /system/post/page")
    public CommonResult<PageResult<PostRespVO>> getPostPage(@QueryMap PostPageReqVO reqVO);


}
