package com.lw.dillon.ui.fx.view.system.logininfor;

import cn.hutool.core.date.DateUtil;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginInforInfoView implements FxmlView<LoginInforInfoViewModel>, Initializable {

    @InjectViewModel
    private LoginInforInfoViewModel viewModel;

    @FXML
    private TextField createTime;

    @FXML
    private TextField id;

    @FXML
    private Button logType;

    @FXML
    private Button result;

    @FXML
    private TextField userAgent;

    @FXML
    private TextField userIp;

    @FXML
    private TextField username;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        id.textProperty().bind(Bindings.createStringBinding(
                () -> viewModel.idProperty().get() + "",
                viewModel.idProperty()));

        viewModel.logTypeProperty().addListener((observableValue, number, t1) -> {
            if (t1 != null) {
                logType.setText(viewModel.getLogTypeMpa().get(t1 + "").getLabel());
                logType.getStyleClass().addAll(viewModel.getLogTypeMpa().get(t1 + "").getColorType());

            }
        });
        viewModel.resultProperty().addListener((observableValue, number, t1) -> {
            if (t1 != null) {
                result.setText(viewModel.getResultMap().get(t1 + "").getLabel());
                result.getStyleClass().addAll(viewModel.getResultMap().get(t1 + "").getColorType());
            }
        });
        userAgent.textProperty().bind(viewModel.userAgentProperty());
        userIp.textProperty().bind(viewModel.userIpProperty());
        username.textProperty().bind(viewModel.usernameProperty());
        createTime.textProperty().bind(Bindings.createStringBinding(
                () -> DateUtil.format(viewModel.createTimeProperty().get(), "yyyy-MM-dd HH:mm:ss"),
                viewModel.createTimeProperty()));
    }


}
