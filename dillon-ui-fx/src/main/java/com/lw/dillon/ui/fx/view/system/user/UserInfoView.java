package com.lw.dillon.ui.fx.view.system.user;

import atlantafx.base.controls.Popover;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.dept.DeptSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import com.lw.dillon.ui.fx.vo.system.post.PostSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import eu.hansolo.fx.charts.voronoi.ArraySet;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.controlsfx.control.CheckComboBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import static atlantafx.base.controls.Popover.ArrowLocation.TOP_CENTER;

public class UserInfoView implements FxmlView<UserInfoViewModel>, Initializable {

    @InjectViewModel
    private UserInfoViewModel viewModel;
    @FXML
    private TextField deptField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField nickNameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField phonenumberField;

    @FXML
    private HBox pwdBox;

    @FXML
    private TextArea remarkArea;

    @FXML
    private VBox rootPane;

    @FXML
    private ComboBox<String> sexCombo;

    @FXML
    private HBox userNameBox;
    @FXML
    private HBox postBox;

    @FXML
    private TextField userNameField;


    private TreeView<DeptSimpleRespVO> deptTreeView;


    private Popover deptTreePopover;


    private CheckComboBox<PostSimpleRespVO> postCheckComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        createView();
        initBinds();
        initListeners();
    }


    private void createView() {
        sexCombo.setConverter(new StringConverter<String>() {
            @Override
            public String toString(String integer) {
                DictDataSimpleRespVO dictDataSimpleRespVO = DictStore.getDictDataMap("system_user_sex").get(integer);
                if (ObjectUtil.isNotEmpty(dictDataSimpleRespVO)) {
                    return dictDataSimpleRespVO.getLabel();
                }
                return integer;
            }

            @Override
            public String fromString(String s) {
                return null;
            }
        });
        createDeptTreeView();
        createPostCheckComboBox();
    }

    private void createPostCheckComboBox() {
        postCheckComboBox = new CheckComboBox<>(viewModel.getPostSimpleRespVOList());
        postCheckComboBox.setConverter(new StringConverter<PostSimpleRespVO>() {
            @Override
            public String toString(PostSimpleRespVO postSimpleRespVO) {
                return postSimpleRespVO.getName();
            }

            @Override
            public PostSimpleRespVO fromString(String s) {
                return null;
            }
        });
        postCheckComboBox.focusedProperty().addListener((o, ov, nv) -> {
            if (nv) {
                postCheckComboBox.show();
            } else {
                postCheckComboBox.hide();
            }
        });
        postCheckComboBox.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(postCheckComboBox, Priority.ALWAYS);
        postBox.getChildren().add(postCheckComboBox);
    }

    private void createDeptTreeView() {
        deptTreeView = new TreeView<DeptSimpleRespVO>();
        deptTreeView.setShowRoot(false);
        deptTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue != null) {
                viewModel.setSelectDept(newValue.getValue());
            }
            if (deptTreePopover != null) {
                deptTreePopover.hide();
            }

        });

        deptTreeView.setCellFactory(new Callback<TreeView<DeptSimpleRespVO>, TreeCell<DeptSimpleRespVO>>() {
            @Override
            public TreeCell<DeptSimpleRespVO> call(TreeView<DeptSimpleRespVO> param) {
                return new TreeCell<>() {
                    @Override
                    protected void updateItem(DeptSimpleRespVO item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setGraphic(new Label(item.getName()));
                        }
                    }
                };
            }
        });

    }

    private void initBinds() {

        userNameBox.visibleProperty().bind(viewModel.editDialogProperty().not());
        userNameBox.managedProperty().bind(userNameBox.visibleProperty());
        pwdBox.visibleProperty().bind(viewModel.editDialogProperty().not());
        pwdBox.managedProperty().bind(pwdBox.visibleProperty());
        nickNameField.textProperty().bindBidirectional(viewModel.nickNameProperty());
        phonenumberField.textProperty().bindBidirectional(viewModel.mobileProperty());
        emailField.textProperty().bindBidirectional(viewModel.emailProperty());
        userNameField.textProperty().bindBidirectional(viewModel.userNameProperty());
        passwordField.textProperty().bindBidirectional(viewModel.passwordProperty());

    }

    private void initListeners() {
        viewModel.sexProperty().addListener((observableValue, old, val) -> {
            sexCombo.getSelectionModel().select(val + "");
        });
        sexCombo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String old, String val) {
                viewModel.sexProperty().set(NumberUtil.parseInt(val));
            }
        });
        viewModel.subscribe("initData", (key, payload) -> {
            initData();
        });
        viewModel.selectDeptProperty().addListener((observableValue, s, t1) -> {
            deptField.setText(t1.getName());
        });
        deptField.setOnMouseClicked(event -> showMenuTreePopover(deptField));

        postCheckComboBox.getCheckModel().getCheckedItems().addListener((ListChangeListener<PostSimpleRespVO>) change -> {


            Set<Long> longSet = new ArraySet<>();
            change.getList().forEach(postSimpleRespVO -> {
                longSet.add(postSimpleRespVO.getId());
            });
            viewModel.getSelPosts().clear();
            viewModel.getSelPosts().addAll(longSet);

        });

    }


    /**
     * 显示弹出窗口菜单树
     *
     * @param source 源
     */
    private void showMenuTreePopover(Node source) {
        if (deptTreePopover == null) {
            deptTreePopover = new Popover(deptTreeView);
            deptTreePopover.setHeaderAlwaysVisible(false);
            deptTreePopover.setDetachable(false);
            deptTreePopover.setArrowLocation(TOP_CENTER);
        }
        deptTreePopover.setPrefWidth(deptField.getWidth() - 40);
//        Bounds bounds = source.localToScreen(source.getBoundsInLocal());
        deptTreePopover.show(source);
    }


    /**
     * 创建根
     */
    private void createDeptTreeData(List<DeptSimpleRespVO> deptSimpleRespVOList) {
        TreeItem<DeptSimpleRespVO> rootItem = generateTreeView(deptSimpleRespVOList, 0l);
        deptTreeView.setRoot(rootItem);


    }

    private TreeItem<DeptSimpleRespVO> generateTreeView(List<DeptSimpleRespVO> deptList, Long parentId) {
        TreeItem<DeptSimpleRespVO> rootNode = new TreeItem<>();
        DeptSimpleRespVO respVO = new DeptSimpleRespVO();
        respVO.setId(0L);
        respVO.setName("顶级部门");
        rootNode.setValue(respVO);
        rootNode.setExpanded(true);
        TreeItem<DeptSimpleRespVO> sel = null;
        for (DeptSimpleRespVO dept : deptList) {
            if (dept.getParentId() == parentId) {
                TreeItem<DeptSimpleRespVO> childNode = new TreeItem<>(dept);
                if (dept.getId() == viewModel.deptIdProperty().get()) {
                    sel = childNode;
                }

                childNode.getChildren().addAll(generateTreeView(deptList, dept.getId()).getChildren());
                rootNode.getChildren().add(childNode);
            }
        }
        if (viewModel.deptIdProperty().get() == 0) {
            sel = rootNode;
        }
        if (sel != null) {
            deptTreeView.getSelectionModel().select(sel);

        }

        return rootNode;
    }



    public void initData() {

        createDeptTreeData(viewModel.getDeptSimpleRespVOList());
        postCheckComboBox.getItems().forEach(postSimpleRespVO -> {


            Set<Long> postIds = viewModel.getUser().getPostIds();
            if (CollUtil.isNotEmpty(postIds) && postIds.contains(postSimpleRespVO.getId())) {
                postCheckComboBox.getCheckModel().toggleCheckState(postSimpleRespVO);
            }
        });
    }


}
