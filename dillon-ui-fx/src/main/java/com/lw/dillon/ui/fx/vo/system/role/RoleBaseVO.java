package com.lw.dillon.ui.fx.vo.system.role;

import lombok.Data;

/**
 * 角色 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class RoleBaseVO {

    private String name;

    private String code;

    private Integer sort;

    private String remark;

}
