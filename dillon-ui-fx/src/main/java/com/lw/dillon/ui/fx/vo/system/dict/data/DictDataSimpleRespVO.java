package com.lw.dillon.ui.fx.vo.system.dict.data;

import lombok.Data;

@Data
public class DictDataSimpleRespVO {

    private String dictType;

    private String value;

    private String label;

    private String colorType;

    private String cssClass;

}
