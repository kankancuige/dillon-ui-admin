package com.lw.dillon.ui.fx.view.system.role;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysRoleFeign;
import com.lw.dillon.ui.fx.vo.system.role.RoleDO;
import com.lw.dillon.ui.fx.vo.system.role.RolePageReqVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class RoleViewModel implements ViewModel {

    private ObjectProperty<LocalDate> startDate = new SimpleObjectProperty();
    private ObjectProperty<LocalDate> endDate = new SimpleObjectProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty Stringcode = new SimpleStringProperty();
    private ObjectProperty<Integer> status = new SimpleObjectProperty<>();
    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);

    private ObservableList<RoleDO> roleDOList = FXCollections.observableArrayList();

    private BooleanProperty loading = new SimpleBooleanProperty(false);

    public RoleViewModel() {
        queryRoleList();
    }

    public void queryRoleList() {

        RolePageReqVO reqVO = new RolePageReqVO();
        reqVO.setPageNo(getPageNum() + 1);
        reqVO.setPageSize(getPageSize());
        reqVO.setName(getName());
        reqVO.setStatus(status.get());
        Map<String, Object> querMap = BeanUtil.beanToMap(reqVO, false, true);
        if (ObjectUtil.isAllNotEmpty(getStartDate(), getEndDate())) {
            String sd = getStartDate().atTime(0, 0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            String ed = getEndDate().atTime(23, 59, 59).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            querMap.put("createTime", new String[]{sd, ed});
        }

        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    loading.set(true);
                    roleDOList.clear();
                }).addSupplierInExecutor(() -> Request.connector(SysRoleFeign.class).getRolePage(querMap).getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    roleDOList.addAll(rel.getList());
                    setTotal(rel.getTotal().intValue());
                })
                .withFinal(() -> loading.set(false))
                .run();

    }

    public void reset() {
        name.setValue(null);
        status.setValue(null);
        endDate.setValue(null);
        startDate.setValue(null);
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public ObjectProperty<LocalDate> startDateProperty() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.set(startDate);
    }

    public LocalDate getEndDate() {
        return endDate.get();
    }

    public ObjectProperty<LocalDate> endDateProperty() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.set(endDate);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getStringcode() {
        return Stringcode.get();
    }

    public StringProperty stringcodeProperty() {
        return Stringcode;
    }

    public void setStringcode(String stringcode) {
        this.Stringcode.set(stringcode);
    }

    public int getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public ObservableList<RoleDO> getRoleDOList() {
        return roleDOList;
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
