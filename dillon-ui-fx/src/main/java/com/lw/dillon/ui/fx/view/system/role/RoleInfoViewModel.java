package com.lw.dillon.ui.fx.view.system.role;

import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysRoleFeign;
import com.lw.dillon.ui.fx.vo.system.role.RoleCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.role.RoleRespVO;
import com.lw.dillon.ui.fx.vo.system.role.RoleUpdateReqVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;


/**
 * 角色信息视图模型类
 *
 * @author wenli
 * @date 2023/11/09
 */
public class RoleInfoViewModel implements ViewModel {

    private ModelWrapper<RoleRespVO> wrapper = new ModelWrapper<>();


    public RoleInfoViewModel() {
        setRole(new RoleRespVO());

    }

    /**
     * 设置系统角色
     *
     * @param respVO 系统作用
     */
    public void setRole(RoleRespVO respVO) {

        this.wrapper.set(respVO);
        this.wrapper.reload();
    }


    /**
     * 获取排序
     *
     * @return IntegerProperty
     */
    public IntegerProperty roleSortProperty() {
        return wrapper.field("sort", RoleRespVO::getSort, RoleRespVO::setSort, 0);
    }

    /**
     * 获取角色编码
     *
     * @return StringProperty
     */
    public StringProperty roleKeyProperty() {
        return wrapper.field("code", RoleRespVO::getCode, RoleRespVO::setCode, "");
    }


    /**
     * 获取状态
     *
     * @return ObjectProperty<Integer>
     */
    public ObjectProperty<Integer> statusProperty() {
        return wrapper.field("status", RoleRespVO::getStatus, RoleRespVO::setStatus, null);
    }

    /**
     * 获取角色名称
     *
     * @return StringProperty
     */
    public StringProperty roleNameProperty() {
        return wrapper.field("name", RoleRespVO::getName, RoleRespVO::setName, "");
    }

    /**
     * 获取备注
     *
     * @return StringProperty
     */
    public StringProperty remarkProperty() {
        return wrapper.field("remark", RoleRespVO::getRemark, RoleRespVO::setRemark, "");
    }


    /**
     * @return {@link CommonResult}<{@link Long}>
     * 创建角色
     * @return CommonResult<Long>
     */
    public CommonResult<Long> createRole() {
        wrapper.commit();

        RoleCreateReqVO createReqVO = new RoleCreateReqVO();
        createReqVO.setCode(roleKeyProperty().getValue());
        createReqVO.setSort(roleSortProperty().getValue());
        createReqVO.setRemark(remarkProperty().getValue());
        createReqVO.setName(roleNameProperty().getValue());
        createReqVO.setStatus(statusProperty().getValue());

        return Request.connector(SysRoleFeign.class).createRole(createReqVO);
    }

    /**
     * 更新角色
     *
     * @return CommonResult<Boolean>
     */
    public CommonResult<Boolean> updateRole() {
        wrapper.commit();
        RoleUpdateReqVO updateReqVO = new RoleUpdateReqVO();
        updateReqVO.setId(wrapper.get().getId());
        updateReqVO.setCode(roleKeyProperty().getValue());
        updateReqVO.setSort(roleSortProperty().getValue());
        updateReqVO.setRemark(remarkProperty().getValue());
        updateReqVO.setName(roleNameProperty().getValue());
        updateReqVO.setStatus(statusProperty().getValue());
        return Request.connector(SysRoleFeign.class).updateRole(updateReqVO);
    }

    /**
     * 获取角色信息
     *
     * @param id 角色ID
     */
    public void getRole(Long id) {

        ProcessChain.create()
                .addSupplierInExecutor(() -> {

                    if (ObjectUtil.isNotEmpty(id)) {
                        return Request.connector(SysRoleFeign.class).getRole(id).getCheckedData();
                    } else {
                        RoleRespVO respVO = new RoleRespVO();
                        respVO.setStatus(0);
                        return respVO;
                    }

                })
                .addConsumerInPlatformThread(r -> {
                    setRole(r);
                }).onException(e -> e.printStackTrace()).run();

    }
}
