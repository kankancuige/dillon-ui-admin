package com.lw.dillon.ui.fx.vo.system.notice;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class NoticeRespVO extends NoticeBaseVO {

    private Long id;

    private LocalDateTime createTime;

}
