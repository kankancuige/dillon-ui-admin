package com.lw.dillon.ui.fx.request.feign.interceptor;

import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.store.AppStore;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class ForwardedForInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {

        template.header("Authorization", "Bearer " + AppStore.getToken());
        if (ObjectUtil.isNotEmpty(AppStore.getTenantId())) {
            template.header("tenant-id", AppStore.getTenantId()+"");
        }else {
            template.header("tenant-id", "1");
        }

    }
}