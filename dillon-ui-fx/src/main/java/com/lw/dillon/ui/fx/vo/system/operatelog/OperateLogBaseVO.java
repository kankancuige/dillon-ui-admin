package com.lw.dillon.ui.fx.vo.system.operatelog;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 操作日志 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class OperateLogBaseVO {

    private String traceId;

    private Long userId;

    private String module;

    private String name;

    private Integer type;

    private String content;

    private Map<String, Object> exts;

    private String requestMethod;

    private String requestUrl;

    private String userIp;

    private String userAgent;

    private String javaMethod;

    private String javaMethodArgs;

    private LocalDateTime startTime;

    private Integer duration;

    private Integer resultCode;

    private String resultMsg;

    private String resultData;

}
