package com.lw.dillon.ui.fx.vo.system.dict.data;

import com.lw.dillon.framework.common.pojo.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class DictDataPageReqVO extends PageParam {

    private String label;

    private String dictType;

    private Integer status;

}
