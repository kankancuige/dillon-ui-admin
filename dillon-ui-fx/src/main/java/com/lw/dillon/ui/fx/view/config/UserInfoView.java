package com.lw.dillon.ui.fx.view.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.vo.system.user.UserProfileRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import de.saxsys.mvvmfx.MvvmFX;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

import static atlantafx.base.theme.Styles.ACCENT;
import static atlantafx.base.theme.Styles.BUTTON_CIRCLE;

/**
 * 快速配置菜单视图
 *
 * @author liwen
 * @date 2022/09/16
 */
public class UserInfoView implements FxmlView<UserInfoViewModel>, Initializable {

    @InjectViewModel
    private UserInfoViewModel userInfoViewModel;

    @FXML
    private VBox root;
    @FXML
    private Button iconBut;

    @FXML
    private Button loginOutBut;
    @FXML
    private Button themeBut;

    @FXML
    private Label userName;

    @FXML
    private Label phonenumber;
    @FXML
    private Label email;
    @FXML
    private Label dept;
    @FXML
    private Label roles;
    @FXML
    private Label post;
    @FXML
    private Label nickName;

    @FXML
    private Label createDate;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        nickName.textProperty().bind(userInfoViewModel.nicknameProperty());
        userName.textProperty().bind(userInfoViewModel.userNameProperty());
        phonenumber.textProperty().bind(userInfoViewModel.phonenumberProperty());
        email.textProperty().bind(userInfoViewModel.emailProperty());
        roles.textProperty().bind(Bindings.createStringBinding(
                () -> {
                    String rolesStr = "";
                    for (UserProfileRespVO.Role role : userInfoViewModel.rolesProperty().get()) {
                        rolesStr += role.getName() + ",";
                    }
                    return rolesStr;
                }, userInfoViewModel.rolesProperty())
        );
        post.textProperty().bind(Bindings.createStringBinding(
                () -> {
                    String postStr = "";
                    for (UserProfileRespVO.Post post : userInfoViewModel.postsProperty().get()) {
                        postStr += post.getName() + ",";
                    }
                    return postStr;
                }, userInfoViewModel.postsProperty())
        );
        dept.textProperty().bind(Bindings.createStringBinding(
                () -> {
                    if (ObjectUtil.isNotEmpty(userInfoViewModel.deptProperty().get())) {
                        return userInfoViewModel.deptProperty().get().getName();
                    } else {
                        return "";
                    }
                }, userInfoViewModel.deptProperty())
        );
        createDate.textProperty().bind(Bindings.createStringBinding(
                () -> DateUtil.format(userInfoViewModel.createTimeProperty().getValue(), "yyyy-MM-dd HH:mm:ss"), userInfoViewModel.createTimeProperty())
        );

        iconBut.getStylesheets().addAll(BUTTON_CIRCLE, ACCENT);
        iconBut.setId("userBut");
        iconBut.setShape(new Circle(120));
        loginOutBut.setOnAction(event -> {

            ProcessChain.create().addSupplierInExecutor(() -> userInfoViewModel.loginOut())
                    .addConsumerInPlatformThread(rel -> {
                        if (rel.isSuccess()) {
                            MvvmFX.getNotificationCenter().publish("showLoginRegister", "显示登录界面");
                        }
                    }).onException(e -> e.printStackTrace()).run();


        });
        themeBut.setOnAction(actionEvent -> {
            MvvmFX.getNotificationCenter().publish("showThemePage", "显示ThemePage");
        });

    }
}
