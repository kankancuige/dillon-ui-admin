package com.lw.dillon.ui.fx.view.system.dict.data;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lw.dillon.ui.fx.enums.ColorTypeEnum;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysDictDataFeign;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataCreateReqVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataUpdateReqVO;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.mapping.ModelWrapper;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Arrays;

public class DictDataInfoViewModel implements ViewModel {

    private ModelWrapper<DictDataRespVO> sysDictWrapper = new ModelWrapper<>();

    private ObservableList<ColorTypeEnum> colorTypeEnumList = FXCollections.observableArrayList();

    public DictDataInfoViewModel() {
        setDictDataRespVO(new DictDataRespVO());

    }

    /**
     * 设置系统角色
     *
     * @param sysDict 系统作用
     */
    public void setDictDataRespVO(DictDataRespVO sysDict) {
        this.sysDictWrapper.set(sysDict);
        this.sysDictWrapper.reload();
    }

    public StringProperty typeProperty() {
        return sysDictWrapper.field("dictType", DictDataRespVO::getDictType, DictDataRespVO::setDictType, null);
    }

    public StringProperty valueProperty() {
        return sysDictWrapper.field("value", DictDataRespVO::getValue, DictDataRespVO::setValue, null);
    }

    public StringProperty cssClassProperty() {
        return sysDictWrapper.field("cssClass", DictDataRespVO::getCssClass, DictDataRespVO::setCssClass, "");
    }

    public StringProperty colorTypeProperty() {
        return sysDictWrapper.field("colorType", DictDataRespVO::getColorType, DictDataRespVO::setColorType, "default");
    }


    public IntegerProperty sortProperty() {
        return sysDictWrapper.field("dictSort", DictDataRespVO::getSort, DictDataRespVO::setSort, 0);
    }

    public ObjectProperty<Integer> statusProperty() {
        return sysDictWrapper.field("status", DictDataRespVO::getStatus, DictDataRespVO::setStatus, null);
    }

    public StringProperty labelProperty() {
        return sysDictWrapper.field("label", DictDataRespVO::getLabel, DictDataRespVO::setLabel, "");
    }

    public StringProperty remarkProperty() {
        return sysDictWrapper.field("remark", DictDataRespVO::getRemark, DictDataRespVO::setRemark, "");
    }

    public ObjectProperty<Long> idProperty() {
        return sysDictWrapper.field("id", DictDataRespVO::getId, DictDataRespVO::setId, null);
    }

    public ObservableList<ColorTypeEnum> getColorTypeEnumList() {
        return colorTypeEnumList;
    }

    public void setColorTypeEnumList(ObservableList<ColorTypeEnum> colorTypeEnumList) {
        this.colorTypeEnumList = colorTypeEnumList;
    }

    public void initData(Long id, String dictType) {
        ProcessChain.create()
                .addRunnableInPlatformThread(() -> {
                    colorTypeEnumList.addAll(Arrays.asList(ColorTypeEnum.values()));
                })
                .addSupplierInExecutor(() -> {
                    if (ObjectUtil.isNotNull(id)) {
                        return Request.connector(SysDictDataFeign.class).getDictData(id).getCheckedData();
                    } else {
                        DictDataRespVO dictDataRespVO = new DictDataRespVO();
                        dictDataRespVO.setDictType(dictType);
                        dictDataRespVO.setStatus(0);
                        return dictDataRespVO;
                    }

                })
                .addConsumerInPlatformThread(rel -> setDictDataRespVO(rel))
                .run();
    }

    /**
     * 保存
     *
     * @param isEdit 是编辑
     * @return {@link Boolean}
     */
    public Boolean save(boolean isEdit) {
        sysDictWrapper.commit();
        DictDataRespVO dictDataRespVO = sysDictWrapper.get();
        Boolean result;
        if (isEdit) {
            DictDataUpdateReqVO reqVO = new DictDataUpdateReqVO();
            BeanUtil.copyProperties(dictDataRespVO, reqVO);
            result = Request.connector(SysDictDataFeign.class).updateDictData(reqVO).isSuccess();

        } else {
            DictDataCreateReqVO reqVO = new DictDataCreateReqVO();
            BeanUtil.copyProperties(dictDataRespVO, reqVO);
            result = Request.connector(SysDictDataFeign.class).createDictData(reqVO).isSuccess();
        }

        return result;
    }


}
