package com.lw.dillon.ui.fx.vo.system.permission;

import lombok.Data;

import java.util.Collections;
import java.util.Set;

@Data
public class PermissionAssignRoleDataScopeReqVO {

    private Long roleId;

//    TODO 这里要多一个枚举校验
    private Integer dataScope;

    private Set<Long> dataScopeDeptIds = Collections.emptySet(); // 兜底

}
