package com.lw.dillon.ui.fx.view.window;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.AuthFeign;
import com.lw.dillon.ui.fx.request.feign.client.SysDictDataFeign;
import com.lw.dillon.ui.fx.store.AppStore;
import com.lw.dillon.ui.fx.store.DictStore;
import com.lw.dillon.ui.fx.vo.system.auth.AuthPermissionInfoRespVO;
import com.lw.dillon.ui.fx.vo.system.dict.data.DictDataSimpleRespVO;
import de.saxsys.mvvmfx.SceneLifecycle;
import de.saxsys.mvvmfx.ViewModel;
import de.saxsys.mvvmfx.utils.commands.Action;
import de.saxsys.mvvmfx.utils.commands.Command;
import de.saxsys.mvvmfx.utils.commands.DelegateCommand;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author wenli
 * @date 2023/10/29
 */
public class MainViewModel implements ViewModel, SceneLifecycle {
    private Command getRoutersCommand;

    public final static String SWITCH_THEME = "switchTheme";

    private final ReadOnlyObjectWrapper<AuthPermissionInfoRespVO.MenuVO> addTab = new ReadOnlyObjectWrapper<>();

    private ObservableList<AuthPermissionInfoRespVO.MenuVO> routerList;
    private SimpleBooleanProperty maximized = new SimpleBooleanProperty(false);
    private SimpleObjectProperty theme = new SimpleObjectProperty();
    private SimpleBooleanProperty showNavigationBar = new SimpleBooleanProperty(true);
    private SimpleBooleanProperty routersUpdate = new SimpleBooleanProperty(false);

    private StringProperty title = new SimpleStringProperty(System.getProperty("app.name"));
    private StringProperty nickName = new SimpleStringProperty();


    public void initialize() {

        getRoutersCommand = new DelegateCommand(() -> new Action() {
            @Override
            protected void action() throws Exception {
                getSimpleDictDataList();
                getPermissionInfo();
            }
        }, true); //Async
        getRoutersCommand.execute();
    }

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }


    /**
     * 在视图中添加
     */
    @Override
    public void onViewAdded() {

//        setTheme(ThemeManager.getInstance().getDefaultTheme());
    }

    /**
     * 在视图中删除
     */
    @Override
    public void onViewRemoved() {
        System.err.println("------remove");
    }


    public boolean isMaximized() {
        return maximized.get();
    }

    public SimpleBooleanProperty maximizedProperty() {
        return maximized;
    }

    public void setMaximized(boolean maximized) {
        this.maximized.set(maximized);
    }


    public boolean isShowNavigationBar() {
        return showNavigationBar.get();
    }

    public SimpleBooleanProperty showNavigationBarProperty() {
        return showNavigationBar;
    }

    public void setShowNavigationBar(boolean showNavigationBar) {
        this.showNavigationBar.set(showNavigationBar);
    }

    public Object getTheme() {
        return theme.get();
    }


    public ObservableList<AuthPermissionInfoRespVO.MenuVO> getRouterList() {
        return routerList;
    }

    public SimpleBooleanProperty routersUpdateProperty() {
        return routersUpdate;
    }

    public ReadOnlyObjectProperty<AuthPermissionInfoRespVO.MenuVO> addTabProperty() {
        return addTab.getReadOnlyProperty();
    }

    public void getPermissionInfo() {
        CommonResult<AuthPermissionInfoRespVO> voCommonResult = Request.connector(AuthFeign.class).getPermissionInfo();

        routerList = FXCollections.observableArrayList(voCommonResult.getData().getMenus());
        Platform.runLater(() -> {
            routersUpdate.setValue(true);
            nickName.set(voCommonResult.getData().getUser().getNickname());
        });

    }

    public String getNickName() {
        return nickName.get();
    }

    public StringProperty nickNameProperty() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName.set(nickName);
    }

    /**
     *
     */
    public void getSimpleDictDataList() {
        CommonResult<List<DictDataSimpleRespVO>> commonResult = Request.connector(SysDictDataFeign.class).getSimpleDictDataList();

        Map<String, Map<String, DictDataSimpleRespVO>> dictDataMap = commonResult.getData().stream()
                .collect(Collectors.groupingBy(DictDataSimpleRespVO::getDictType,
                        Collectors.toMap(DictDataSimpleRespVO::getValue, dictData -> dictData)));


        Platform.runLater(() -> DictStore.setAllDictDataMap(dictDataMap));

    }

    public void addTab(AuthPermissionInfoRespVO.MenuVO obj) {
        addTab.set(Objects.requireNonNull(obj));
    }
}
