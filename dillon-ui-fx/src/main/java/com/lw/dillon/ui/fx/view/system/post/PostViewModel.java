package com.lw.dillon.ui.fx.view.system.post;

import com.lw.dillon.framework.common.pojo.CommonResult;
import com.lw.dillon.ui.fx.request.Request;
import com.lw.dillon.ui.fx.request.feign.client.SysPostFeign;
import com.lw.dillon.ui.fx.vo.system.post.PostPageReqVO;
import com.lw.dillon.ui.fx.vo.system.post.PostRespVO;
import de.saxsys.mvvmfx.ViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PostViewModel implements ViewModel {

    private ObservableList<PostRespVO> postList = FXCollections.observableArrayList();

    private SimpleIntegerProperty total = new SimpleIntegerProperty(0);
    private ObjectProperty<Integer> status = new SimpleObjectProperty<>();
    private StringProperty postName = new SimpleStringProperty("");
    private StringProperty postCode = new SimpleStringProperty("");

    private IntegerProperty pageNum = new SimpleIntegerProperty(0);
    private IntegerProperty pageSize = new SimpleIntegerProperty(10);
    private BooleanProperty loading = new SimpleBooleanProperty(false);

    public PostViewModel() {
        getPostPage();
    }

    public void getPostPage() {
        PostPageReqVO postPageReqVO = new PostPageReqVO();
        postPageReqVO.setName(postName.getValue());
        postPageReqVO.setCode(postCode.getValue());
        postPageReqVO.setStatus(status.getValue());
        postPageReqVO.setPageNo(pageNum.get() + 1);
        postPageReqVO.setPageSize(pageSize.get());

        ProcessChain.create()
                .addRunnableInPlatformThread(() -> loading.set(true))
                .addSupplierInExecutor(() -> Request.connector(SysPostFeign.class).getPostPage(postPageReqVO).getCheckedData())
                .addConsumerInPlatformThread(rel -> {
                    postList.clear();
                    postList.addAll(rel.getList());
                    total.set(rel.getTotal().intValue());
                }).withFinal(() -> loading.set(false)).onException(e -> e.printStackTrace())
                .run();
    }


    public void reset() {
        postCode.setValue(null);
        postName.setValue(null);
        status.setValue(null);
        getPostPage();
    }

    public CommonResult<Boolean> deletePost(Long id) {
        return Request.connector(SysPostFeign.class).deletePost(id);
    }

    public ObservableList<PostRespVO> getPostList() {
        return postList;
    }

    public void setPostList(ObservableList<PostRespVO> postList) {
        this.postList = postList;
    }

    public int getTotal() {
        return total.get();
    }

    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    public void setTotal(int total) {
        this.total.set(total);
    }

    public Integer getStatus() {
        return status.get();
    }

    public ObjectProperty<Integer> statusProperty() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status.set(status);
    }

    public String getPostName() {
        return postName.get();
    }

    public StringProperty postNameProperty() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName.set(postName);
    }

    public String getPostCode() {
        return postCode.get();
    }

    public StringProperty postCodeProperty() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode.set(postCode);
    }

    public int getPageNum() {
        return pageNum.get();
    }

    public IntegerProperty pageNumProperty() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum.set(pageNum);
    }

    public int getPageSize() {
        return pageSize.get();
    }

    public IntegerProperty pageSizeProperty() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize.set(pageSize);
    }

    public boolean isLoading() {
        return loading.get();
    }

    public BooleanProperty loadingProperty() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading.set(loading);
    }
}
