package com.lw.dillon.ui.fx.view.system.menu;

import atlantafx.base.controls.Popover;
import atlantafx.base.controls.ToggleSwitch;
import cn.hutool.core.util.NumberUtil;
import com.lw.dillon.ui.fx.vo.system.menu.MenuSimpleRespVO;
import de.saxsys.mvvmfx.FxmlView;
import de.saxsys.mvvmfx.InjectViewModel;
import io.datafx.core.concurrent.ProcessChain;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static atlantafx.base.controls.Popover.ArrowLocation.TOP_CENTER;

/**
 * 视图菜单对话框
 *
 * @author wenli
 * @date 2023/02/15
 */
public class MenuDialogView implements FxmlView<MenuDialogViewModel>, Initializable {

    @InjectViewModel
    private MenuDialogViewModel viewModel;


    private TreeView<MenuSimpleRespVO> menuTreeView;

    private Popover menuTreePopover;


    @FXML
    private HBox alwaysShowBox;

    @FXML
    private ToggleSwitch alwaysSwitch;

    @FXML
    private ToggleButton butButon;

    @FXML
    private HBox cacheBox;

    @FXML
    private ToggleSwitch cacheSwitch;

    @FXML
    private HBox componentBox;

    @FXML
    private TextField componentClassField;

    @FXML
    private TextField componentField;

    @FXML
    private HBox componentNameBox;

    @FXML
    private TextField componentNameField;

    @FXML
    private HBox componentPathBox;

    @FXML
    private ToggleButton dirButton;

    @FXML
    private HBox iconBox;

    @FXML
    private TextField iconField;

    @FXML
    private ToggleButton menuButton;

    @FXML
    private TextField menuNameField;

    @FXML
    private TextField parentIdCombo;

    @FXML
    private HBox pathBox;

    @FXML
    private TextField pathField;

    @FXML
    private HBox permissionBox;

    @FXML
    private TextField permissionField;

    @FXML
    private Spinner<Integer> sortSpinner;

    @FXML
    private RadioButton statusRadio1;

    @FXML
    private RadioButton statusRadio2;

    @FXML
    private ToggleGroup toggleGroup1;

    @FXML
    private ToggleGroup toggleGroup2;

    @FXML
    private HBox visibleBox;

    @FXML
    private ToggleSwitch visibleSwitch;

    /**
     * 初始化
     *
     * @param url            url
     * @param resourceBundle 资源包
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        initView();
        initBinds();
        initListeners();
        initMenuTureeData();
    }

    private void initView() {
        sortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0));
        sortSpinner.getValueFactory().setValue(0);
        visibleSwitch.selectedProperty().addListener((observableValue, oldV, val) -> {
            if (val) {
                visibleSwitch.setText("显示");
            } else {
                visibleSwitch.setText("隐藏");
            }
        });
        alwaysSwitch.selectedProperty().addListener((observableValue, oldV, val) -> {
            if (val) {
                alwaysSwitch.setText("总是");
            } else {
                alwaysSwitch.setText("不是");
            }
        });

        cacheSwitch.selectedProperty().addListener((observableValue, oldV, val) -> {
            if (val) {
                cacheSwitch.setText("缓存");
            } else {
                cacheSwitch.setText("不缓存");
            }
        });
        createMenuTreeView();
    }

    private void initListeners() {

        parentIdCombo.setOnMouseClicked(event -> showMenuTreePopover(parentIdCombo));

    }

    private void createMenuTreeView() {
        menuTreeView = new TreeView<MenuSimpleRespVO>();
        menuTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue != null) {
                viewModel.setSelectMenu(newValue.getValue());
            }
            if (menuTreePopover != null) {
                menuTreePopover.hide();
            }

        });

        menuTreeView.setCellFactory(new Callback<TreeView<MenuSimpleRespVO>, TreeCell<MenuSimpleRespVO>>() {
            @Override
            public TreeCell<MenuSimpleRespVO> call(TreeView<MenuSimpleRespVO> param) {
                return new TreeCell<>() {
                    @Override
                    protected void updateItem(MenuSimpleRespVO item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });

    }


    private void initBinds() {

        sortSpinner.getValueFactory().valueProperty().bindBidirectional(viewModel.sortProperty().asObject());
        parentIdCombo.textProperty().bind(Bindings.createStringBinding(() -> viewModel.selectMenuProperty().getValue().getName(), viewModel.selectMenuProperty()));

        iconBox.visibleProperty().bind(Bindings.or(dirButton.selectedProperty(), menuButton.selectedProperty()));
        iconBox.managedProperty().bind(iconBox.visibleProperty());

        pathBox.visibleProperty().bind(Bindings.or(dirButton.selectedProperty(), menuButton.selectedProperty()));
        pathBox.managedProperty().bind(pathBox.visibleProperty());

        pathBox.visibleProperty().bind(Bindings.or(dirButton.selectedProperty(), menuButton.selectedProperty()));
        pathBox.managedProperty().bind(pathBox.visibleProperty());

        visibleBox.visibleProperty().bind(Bindings.or(dirButton.selectedProperty(), menuButton.selectedProperty()));
        visibleBox.managedProperty().bind(visibleBox.visibleProperty());

        alwaysShowBox.visibleProperty().bind(Bindings.or(dirButton.selectedProperty(), menuButton.selectedProperty()));
        alwaysShowBox.managedProperty().bind(alwaysShowBox.visibleProperty());

        permissionBox.visibleProperty().bind(Bindings.or(butButon.selectedProperty(), menuButton.selectedProperty()));
        permissionBox.managedProperty().bind(permissionBox.visibleProperty());

        componentBox.visibleProperty().bind(menuButton.selectedProperty());
        componentBox.managedProperty().bind(componentBox.visibleProperty());

        componentNameBox.visibleProperty().bind(menuButton.selectedProperty());
        componentNameBox.managedProperty().bind(componentNameBox.visibleProperty());

        componentPathBox.visibleProperty().bind(menuButton.selectedProperty());
        componentPathBox.managedProperty().bind(componentPathBox.visibleProperty());

        cacheBox.visibleProperty().bind(menuButton.selectedProperty());
        cacheBox.managedProperty().bind(cacheBox.visibleProperty());


        menuNameField.textProperty().bindBidirectional(viewModel.menuNameProperty());
        iconField.textProperty().bindBidirectional(viewModel.iconProperty());
        pathField.textProperty().bindBidirectional(viewModel.pathProperty());
        componentField.textProperty().bindBidirectional(viewModel.componentProperty());
        componentNameField.textProperty().bindBidirectional(viewModel.componentNameProperty());
        componentClassField.textProperty().bindBidirectional(viewModel.componentClassProperty());
        permissionField.textProperty().bindBidirectional(viewModel.permsProperty());

        dirBind(dirButton.selectedProperty(), viewModel.menuTypeProperty(), 1);
        dirBind(menuButton.selectedProperty(), viewModel.menuTypeProperty(), 2);
        dirBind(butButon.selectedProperty(), viewModel.menuTypeProperty(), 3);

        dirBind(statusRadio1.selectedProperty(), viewModel.statusProperty(), 0);
        dirBind(statusRadio2.selectedProperty(), viewModel.statusProperty(), 1);
        visibleSwitch.selectedProperty().bindBidirectional(viewModel.visibleProperty());
        alwaysSwitch.selectedProperty().bindBidirectional(viewModel.alwaysShowProperty());
        cacheSwitch.selectedProperty().bindBidirectional(viewModel.keepAliveProperty());

    }

    private TreeItem selItem = null;

    /**
     * 创建根
     */
    private void createRoot() {
        MenuSimpleRespVO menuSimpleRespVO = new MenuSimpleRespVO();
        menuSimpleRespVO.setName("主类目");
        menuSimpleRespVO.setId(0L);
        List<MenuSimpleRespVO> menuList = viewModel.getMenuSimpleRespVOObservableList(); // Replace with your actual list

        TreeItem<MenuSimpleRespVO> rootItem = new TreeItem<>(menuSimpleRespVO);
        rootItem.setExpanded(true);

        selItem = rootItem;

        buildTreeView(rootItem, menuList, 0); // Start building the tree recursively
        menuTreeView.setRoot(rootItem);
        menuTreeView.getSelectionModel().select(selItem);


    }

    private void buildTreeView(TreeItem<MenuSimpleRespVO> parentItem, List<MenuSimpleRespVO> menuList, long parentId) {
        for (MenuSimpleRespVO menu : menuList) {
            if (menu.getParentId() == parentId) {
                TreeItem<MenuSimpleRespVO> item = new TreeItem<>(menu);
                parentItem.getChildren().add(item);
                if (NumberUtil.equals(menu.getId(), viewModel.getSelectMenu().getId())) {
                    selItem = item;
                }

                buildTreeView(item, menuList, menu.getId()); // Recursively build children
            }
        }
    }



    /**
     * 显示弹出窗口菜单树
     *
     * @param source 源
     */
    private void showMenuTreePopover(Node source) {
        if (menuTreePopover == null) {
            menuTreePopover = new Popover(menuTreeView);
            menuTreePopover.setHeaderAlwaysVisible(false);
            menuTreePopover.setDetachable(false);
            menuTreePopover.setArrowLocation(TOP_CENTER);
        }
        menuTreeView.setPrefWidth(parentIdCombo.getWidth() - 40);
//        Bounds bounds = source.localToScreen(source.getBoundsInLocal());
        menuTreePopover.show(source);
    }

    /**
     * 初始化菜单turee
     * init弥尼树
     */
    private void initMenuTureeData() {
        ProcessChain.create().addRunnableInExecutor(() -> viewModel.getSimpleMenuList()).addRunnableInPlatformThread(() -> createRoot()).onException(e -> e.printStackTrace()).run();
    }

    public void dirBind(BooleanProperty booleanProperty, IntegerProperty integerProperty, Integer mentType) {
        // 监听BooleanProperty的变化，然后更新IntegerProperty
        booleanProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                integerProperty.set(mentType);
            }

        });

        // 监听IntegerProperty的变化，然后更新BooleanProperty
        integerProperty.addListener((observable, oldValue, newValue) -> {
            if (NumberUtil.equals(newValue, mentType)) {
                booleanProperty.set(true);
            }
        });
    }

    public void dirBind(BooleanProperty booleanProperty, BooleanProperty booleanProperty2, boolean b) {
        booleanProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                booleanProperty2.set(b);
            }

        });

        booleanProperty2.addListener((observable, oldValue, newValue) -> {
            if (newValue == b) {
                booleanProperty.set(true);
            }
        });
    }


    public void mutuallyeExclusiveBind(BooleanProperty booleanProperty1, BooleanProperty booleanProperty2) {
        // 添加一个自定义的ChangeListener以实现互反双向绑定
        booleanProperty1.addListener((observable, oldValue, newValue) -> {
            booleanProperty2.set(!newValue);
        });

        booleanProperty2.addListener((observable, oldValue, newValue) -> {
            booleanProperty1.set(!newValue);
        });

    }


    private void bind(BooleanProperty prop1, BooleanProperty prop2) {
        // 监听prop1的变化，然后更新prop2
        prop1.addListener((observable, oldValue, newValue) -> {
            prop2.set(!newValue);
        });

        // 监听prop2的变化，然后更新prop1
        prop2.addListener((observable, oldValue, newValue) -> {
            prop1.set(!newValue);
        });
    }

}
